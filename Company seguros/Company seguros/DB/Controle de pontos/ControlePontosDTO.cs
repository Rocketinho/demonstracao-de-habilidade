﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Controle_de_pontos
{
    class ControlePontosDTO
    {
        public int Id{ get; set; }
        public DateTime Data{ get; set; }
        public string Entrada{ get; set; }
        public string Almoco_Saida{ get; set; }
        public string Almoco_Retorno{ get; set; }
        public string Saida{ get; set; }
        public int Id_funcionario{ get; set; }


    }
}
