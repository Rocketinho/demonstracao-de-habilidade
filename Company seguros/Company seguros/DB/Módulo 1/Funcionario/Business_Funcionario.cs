﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario
{
    public class Business_Funcionario
    {
        Database_Funcionario db = new Database_Funcionario();
        public List<DTO_Funcionario> Listar()
        {
          return db.Listar();
           
        }
            public DTO_Funcionario Logar(DTO_Funcionario dto)
        {
            return db.Logar(dto);
        }
        public bool VerificarSenha(string senha)
        {
            return db.VerificarSenha(senha);
        }
        public void Alterar(DTO_Funcionario dto)
        {
            // Nome Nulo
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("O nome não pode ser nulo");
            }
            else
            {
                if (dto.RG == null)
                {
                    throw new ArgumentException("O RG não pode ser nulo");

                }
                else
                {
                    if (dto.CPF == null)
                    {
                        throw new ArgumentException("O CPF não pode ser nulo");

                    }
                    else
                    {
                        if (dto.Sexo == null)
                        {
                            throw new ArgumentException("O campo Sexo não pode ser nulo");

                        }
                        else
                        {
                            if (dto.Salario == 0)
                            {
                                throw new ArgumentException("O Salário não pode ser nulo");

                            }
                           
                                else
                                {
                                    if (dto.Usuario.Trim() == string.Empty)
                                    {
                                        throw new ArgumentException("Usuario não pode ser nulo");

                                    }
                                    else
                                    {
                                        if (dto.Senha.Trim() == string.Empty)
                                        {
                                            throw new ArgumentException("Senha não pode ser nulo");

                                        }
                                        else
                                        {

                                             db.Alterar(dto);


                                        }
                                    }
                                }


                            

                        }
                    }
                }


            }




        }
        public void Remover(DTO_Funcionario dto)
        {
            db.Remover(dto);

        }
        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            List<DTO_Funcionario> lista =  db.Consultar(dto);
            return lista;
        }
        public int Salvar(DTO_Funcionario dto)
        {
            // Nome Nulo
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("O nome não pode ser nulo");
            }
            else
            {
                if (dto.RG == null)
                {
                    throw new ArgumentException("O RG não pode ser nulo");

                }
                else
                {
                    if (dto.CPF == null)
                    {
                        throw new ArgumentException("O CPF não pode ser nulo");

                    }
                    else
                    {
                        if (dto.Sexo == null)
                        {
                            throw new ArgumentException("O campo Sexo não pode ser nulo");

                        }
                        else
                        {
                            if (dto.Salario == 0)
                            {
                                throw new ArgumentException("O Salário não pode ser nulo");

                            }
                            else
                            {
                                Validacao.Validacao validacao = new Validacao.Validacao();
                                string resp = validacao.ConsultarCPFBanco(dto.CPF);
                                if (resp == dto.CPF)
                                {
                                    throw new ArgumentException("Esse CPF já está cadastrado");

                                }
                                else
                                {
                                    if (dto.Usuario.Trim() == string.Empty)
                                    {
                                        throw new ArgumentException("Usuario não pode ser nulo");

                                    }
                                    else
                                    {
                                        if (dto.Senha.Trim() == string.Empty)
                                        {
                                            throw new ArgumentException("Senha não pode ser nulo");

                                        }
                                        else
                                        {

                                            return db.Salvar(dto);


                                        }
                                    }
                                }


                            }

                        }
                    }
                }


            }



        }
    }
}
