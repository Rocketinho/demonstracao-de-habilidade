﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario
{
    public class DTO_Funcionario
    {
        public int Id{ get; set; }

        public string Nome{ get; set; }

        public string RG { get; set; }

        public string CPF { get; set; }

        public string Rua { get; set; }

        public string Bairro { get; set; }

        public string Número { get; set; }

        public string Email { get; set; }

        public string Telefone { get; set; }

        public string Cidade { get; set; }

        public decimal Salario { get; set; }

        public decimal Transporte { get; set; }

        public decimal Refeicao { get; set; }

        public decimal Alimentacao { get; set; }

        public decimal Convenio { get; set; }

        public DateTime Nascimento { get; set; }

        public string Sexo { get; set; }

        public decimal FGTS { get; set; }

        public decimal INSS { get; set; }

        public decimal Horas_Extras { get; set; }

        public string Usuario { get; set; }

        public string Senha { get; set; }



    }
}
