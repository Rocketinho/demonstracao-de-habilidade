﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario
{
    public class Database_Funcionario
    {
        Database db = new Database();

        public int Salvar(DTO_Funcionario dto)
        {
            string script =
                @"INSERT INTO tb_funcionario(nm_funcionario, ds_RG, ds_CPF, tp_sexo, dt_nascimento, vl_vale_transporte, vl_vale_refeicao, vl_vale_alimentacao, vl_vale_convenio, vl_salario, nm_cidade , nr_residencia, ds_email, ds_telefone, ds_rua, ds_bairro, ds_usuario, ds_senha)
                  VALUES (@nm_funcionario, @ds_RG, @ds_CPF, @tp_sexo, @dt_nascimento ,@vl_vale_transporte, @vl_vale_refeicao, @vl_vale_alimentacao, @vl_vale_convenio, @vl_salario, @nm_cidade, @nr_residencia, @ds_email, @ds_telefone, @ds_rua, @ds_bairro, @ds_usuario, @ds_senha) ";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.Transporte));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.Alimentacao));
            parms.Add(new MySqlParameter("vl_vale_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));         
            parms.Add(new MySqlParameter("nr_residencia", dto.Número));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_rua", dto.Rua));
            parms.Add(new MySqlParameter("ds_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));



            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(DTO_Funcionario dto)
        {
            string script =
                @"Update tb_funcionario SET nm_funcionario = @nm_funcionario,
                                            ds_RG = @ds_RG,
                                            ds_CPF = @ds_CPF,
                                            dt_nascimento = @dt_nascimento,
                                            tp_sexo = @tp_sexo,
                                            vl_vale_transporte = @vl_vale_transporte,
                                            vl_vale_refeicao = @vl_vale_refeicao,
                                            vl_vale_alimentacao = @vl_vale_alimentacao,
                                            vl_vale_convenio = @vl_vale_convenio,
                                            vl_salario = @vl_salario,
                                            ds_usuario = @ds_usuario,
                                            ds_senha = @ds_senha,           
                                            nm_cidade = @nm_cidade,                                          
                                            nr_residencia = @nr_residencia,                                        
                                            ds_email = @ds_email,
                                            ds_telefone = @ds_telefone
                                         
                   WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));
            parms.Add(new MySqlParameter("nm_funcionario", dto.Nome));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("tp_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("vl_vale_transporte", dto.Transporte));
            parms.Add(new MySqlParameter("vl_vale_refeicao", dto.Refeicao));
            parms.Add(new MySqlParameter("vl_vale_alimentacao", dto.Alimentacao));
            parms.Add(new MySqlParameter("vl_vale_convenio", dto.Convenio));
            parms.Add(new MySqlParameter("vl_salario", dto.Salario));
            
            parms.Add(new MySqlParameter("nm_cidade", dto.Cidade));
    
            parms.Add(new MySqlParameter("nr_residencia", dto.Número));
       
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parms.Add(new MySqlParameter("ds_senha", dto.Senha));



            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(DTO_Funcionario dto)
        {
            string script = @"DELETE FROM tb_funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.Id));

            Database db = new Database();
            
            db.ExecuteInsertScript(script, parms);
        }
        public List<DTO_Funcionario> Consultar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
            WHERE nm_funcionario like @nm_funcionario AND 
                  ds_RG like @ds_RG AND
                  ds_CPF like @ds_CPF";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_funcionario", "%" + dto.Nome + "%"));
            parms.Add(new MySqlParameter("ds_RG", "%" + dto.RG + "%"));
            parms.Add(new MySqlParameter("ds_CPF", "%" + dto.CPF + "%"));
           Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            while (reader.Read())
            {
                DTO_Funcionario dt = new DTO_Funcionario();
                dt.Id = reader.GetInt32("id_funcionario");
                dt.Nome = reader.GetString("nm_funcionario");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Telefone = reader.GetString("ds_telefone");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Email = reader.GetString("ds_email");
                lista.Add(dt);

            }
            reader.Close();
            return lista;


        }
        public List<DTO_Funcionario> Listar()
        {
            string script = @"SELECT * FROM tb_funcionario
           ";
          
          Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_Funcionario> lista = new List<DTO_Funcionario>();
            while (reader.Read())
            {
                DTO_Funcionario dt = new DTO_Funcionario();
                dt.Id = reader.GetInt32("id_funcionario");
                dt.Nome = reader.GetString("nm_funcionario");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Telefone = reader.GetString("ds_telefone");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Email = reader.GetString("ds_email");
                lista.Add(dt);

            }
            reader.Close();
            return lista;


        }


        public DTO_Funcionario Logar(DTO_Funcionario dto)
        {
            string script = @"SELECT * FROM tb_funcionario
                                      WHERE ds_usuario = @ds_usuario
                                        and ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_usuario", dto.Usuario));
            parm.Add(new MySqlParameter("ds_senha", dto.Senha));


            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            DTO_Funcionario fora = null;

            if (reader.Read())
            {
                
                fora = new DTO_Funcionario();
                fora.Id = reader.GetInt32("id_funcionario");
                fora.Nome = reader.GetString("nm_funcionario");
                fora.RG = reader.GetString("ds_RG");
                fora.CPF = reader.GetString("ds_CPF");
                fora.Nascimento = reader.GetDateTime("dt_nascimento");
                fora.Sexo = reader.GetString("tp_sexo");
                fora.Transporte = reader.GetDecimal("vl_vale_transporte");
                fora.Refeicao = reader.GetDecimal("vl_vale_refeicao");
                fora.Alimentacao = reader.GetDecimal("vl_vale_alimentacao");
                fora.Convenio = reader.GetDecimal("vl_vale_convenio");
                fora.Salario = reader.GetDecimal("vl_salario");
                fora.Cidade = reader.GetString("nm_cidade");
                fora.Rua = reader.GetString("ds_rua");
                fora.Bairro = reader.GetString("ds_bairro");
                fora.Número = reader.GetString("nr_residencia");
                fora.Email = reader.GetString("ds_email");
                fora.Telefone = reader.GetString("ds_telefone");
                fora.Usuario = reader.GetString("ds_usuario");
                fora.Senha = reader.GetString("ds_senha");

            }

            reader.Close();
            return fora;
        }

        public bool VerificarSenha(string senha)
        {
            string script = @"SELECT * FROM tb_funcionario
                                       WHERE ds_senha = @ds_senha";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_senha", senha));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
    }

