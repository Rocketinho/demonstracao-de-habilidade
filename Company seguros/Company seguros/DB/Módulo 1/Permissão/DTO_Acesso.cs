﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario.Permissão
{
   public class DTO_Acesso
    {
        public int ID { get; set; }

        public int ID_Funcionario { get; set; }

        public int ID_Departamento { get; set; }

        public bool Salvar { get; set; }

        public bool Remover { get; set; }

        public bool Alterar { get; set; }

        public bool Consultar { get; set; }
    }
}
