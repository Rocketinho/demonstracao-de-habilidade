﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario.Permissão
{
    public class Database_Acesso
    {
        public int Salvar(DTO_Acesso dto)
        {
            string script =
           @"INSERT INTO tb_nivel_acesso(id_funcionario, id_departamento, bl_salvar, bl_remover, bl_alterar, bl_consultar)
            VALUES(@id_funcionario, @id_departamento, @bl_salvar, @bl_remover, @bl_alterar, @bl_consultar)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
        public void Alterar(DTO_Acesso dto)
        {
            string script =
                @"UPDATE tb_nivel_acesso SET 
                                             id_departamento = @id_departamento,
                                             bl_salvar = @bl_salvar,
                                             bl_remover = @bl_remover,
                                             bl_alterar = @bl_alterar,
                                             bl_consultar = @bl_consultar
                WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            parms.Add(new MySqlParameter("id_departamento", dto.ID_Departamento));
            parms.Add(new MySqlParameter("bl_salvar", dto.Salvar));
            parms.Add(new MySqlParameter("bl_remover", dto.Remover));
            parms.Add(new MySqlParameter("bl_alterar", dto.Alterar));
            parms.Add(new MySqlParameter("bl_consultar", dto.Consultar));
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public void Remover(DTO_Acesso dto)
        {
            string script =
                @"DELETE FROM tb_nivel_acesso WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }
        public List<DTO_Acesso> Consultar(DTO_Acesso dto)
        {
            string script =
                @"SELECT * FROM tb_nivel_acesso WHERE id_funcionario = @id_funcionario";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_funcionario", dto.ID_Funcionario));
            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Acesso> lista = new List<DTO_Acesso>();
            while (reader.Read())
            {
                DTO_Acesso dt = new DTO_Acesso();
                dt.ID = reader.GetInt32("id_nivel_acesso");
                dt.ID_Funcionario = reader.GetInt32("id_funcionario");
                dt.Salvar = reader.GetBoolean("bl_salvar");
                dt.Remover = reader.GetBoolean("bl_remover");
                dt.Alterar = reader.GetBoolean("bl_alterar");
                dt.Consultar = reader.GetBoolean("bl_consultar");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
    }
}
