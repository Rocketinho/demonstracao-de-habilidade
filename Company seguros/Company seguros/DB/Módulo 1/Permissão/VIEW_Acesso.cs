﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Funcionario
{
    public class VIEW_Acesso
    {
        public class VIEW_Acesso_Funcionario
        {
            public int ID { get; set; }
            public int ID_Funcionario { get; set; }
            public int ID_Departamento { get; set; }
            public string Usuario { get; set; }
            public string Senha { get; set; }

            public bool Remover { get; set; }
            public bool Alterar { get; set; }
            public bool Consultar { get; set; }
            public bool Salvar { get; set; }

            //Fica ridiculo, mas estámos sem tempo!

            public VIEW_Acesso_Funcionario Logar(VIEW_Acesso_Funcionario dto)
            {
                Database db = new Database();

                string script = @"SELECT * FROM view_acesso_funcionario
                                      WHERE ds_usuario =@ds_usuario
                                      AND   ds_senha   =@ds_senha";

                List<MySqlParameter> parm = new List<MySqlParameter>();
                parm.Add(new MySqlParameter("ds_usuario", dto.Usuario));
                parm.Add(new MySqlParameter("ds_senha", dto.Senha));

                MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
                VIEW_Acesso_Funcionario fora = null;

                if (reader.Read())
                {
                    fora = new VIEW_Acesso_Funcionario();
                    fora.ID = reader.GetInt32("id_permissao");
                    fora.ID_Funcionario = reader.GetInt32("id_funcionario");
                    fora.ID_Departamento = reader.GetInt32("id_departamento");
                    fora.Usuario = reader.GetString("ds_usuario");
                    fora.Senha = reader.GetString("ds_senha");

                    fora.Alterar = reader.GetBoolean("bl_alterar");
                    fora.Salvar = reader.GetBoolean("bl_salvar");
                    fora.Remover = reader.GetBoolean("bl_remover");
                    fora.Consultar = reader.GetBoolean("bl_consultar");
                }

                reader.Close();
                return fora;
            }
        }
    }
}
