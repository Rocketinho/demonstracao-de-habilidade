﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Compra
{
    public class Business_Compra
    {

        //Método de escopo
        Database_Compra db = new Database_Compra();

        //Método de Salvar
        public int Salvar(DTO_Compra dto)
        {
            return db.Salvar(dto);
        }

        //Método de Remover
        public void Remover(int id)
        {
            db.Remover(id);
        }

        //Método de Alterar
        public void Alterar(DTO_Compra dto)
        {
            db.Alterar(dto);
        }

        //Método de Listar (Sem filtro)
        public List<DTO_Compra> Listar()
        {
            List<DTO_Compra> list = db.Listar();
            return list;
        }

        //Método de Consultar (Com filtro)
        //public List<DTO_Compra> Consultar(DTO_Compra dto)
        //{
        //    List<DTO_Compra> list = db.Consultar(dto);
        //    return list;
        //}
    }
}
