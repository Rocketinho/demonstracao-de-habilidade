﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Fornecedor
{
    public class Database_Fornecedor
    {
        public int Salvar(DTO_Fornecedor dto)
        {
            string script =
            @"INSERT INTO tb_fornecedor
            (
                ds_razao_social,
                ds_cnpj,
                ds_telefone,
                ds_email,
                ds_endereco,
                ds_cep,
                ds_complemento,
                nr_casa,
                ds_uf,
                ds_cidade
            )
            VALUES
            (
                @ds_razao_social,
                @ds_cnpj,
                @ds_telefone,
                @ds_email,
                @ds_endereco,
                @ds_cep,
                @ds_complemento,
                @nr_casa,
                @ds_uf,
                @ds_cidade
            )";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_razao_social", dto.Razao_Social));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_casa", dto.Numero_Casa));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));

           Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parm);
        }
        public void Remover(int dto)
        {
            string script = @"DELETE FROM tb_fornecedor
                                    WHERE id_fornecedor = @id_fornecedor";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public void Alterar(DTO_Fornecedor dto)
        {
            string script =
                @"UPDATE tb_fornecedor
                     SET ds_razao_social    =@ds_razao_social,
                         ds_cnpj            =@ds_cnpj,
                         ds_telefone        =@ds_telefone,
                         ds_email           =@ds_email,
                         ds_endereco        =@ds_endereco,
                         ds_cep             =@ds_cep,
                         ds_complemento     =@ds_complemento,
                         nr_casa            =@nr_casa,
                         ds_uf              =@ds_uf,
                         ds_cidade          =@ds_cidade
                  WHERE  id_fornecedor      =@id_fornecedor";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("id_fornecedor", dto.ID));
            parm.Add(new MySqlParameter("ds_razao_social", dto.Razao_Social));
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));
            parm.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parm.Add(new MySqlParameter("ds_email", dto.Email));
            parm.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parm.Add(new MySqlParameter("ds_cep", dto.CEP));
            parm.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parm.Add(new MySqlParameter("nr_casa", dto.Numero_Casa));
            parm.Add(new MySqlParameter("ds_uf", dto.UF));
            parm.Add(new MySqlParameter("ds_cidade", dto.Cidade));

            Database db = new Database();
            db.ExecuteInsertScript(script, parm);
        }
        public List<DTO_Fornecedor> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";
            List<MySqlParameter> parm = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Fornecedor> fora = new List<DTO_Fornecedor>();

            while (reader.Read())
            {
                DTO_Fornecedor dentro = new DTO_Fornecedor();
                dentro.ID = reader.GetInt32("id_fornecedor");
                dentro.Razao_Social = reader.GetString("ds_razao_social");
                dentro.CNPJ = reader.GetString("ds_cnpj");
                dentro.Telefone = reader.GetString("ds_telefone");
                dentro.Email = reader.GetString("ds_email");
                dentro.Endereco = reader.GetString("ds_endereco");
                dentro.CEP = reader.GetString("ds_cep");
                dentro.Complemento = reader.GetString("ds_complemento");
                dentro.Numero_Casa = reader.GetDecimal("nr_casa");
                dentro.UF = reader.GetString("ds_uf");
                dentro.Cidade = reader.GetString("ds_cidade");

                fora.Add(dentro);
            }

            reader.Close();
            return fora;
        }

        public List<DTO_Fornecedor> Consultar(DTO_Fornecedor dto)
        {
            string script = @"SELECT * FROM tb_fornecedor
                                      WHERE ds_razao_social LIKE @ds_razao_social";
            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_razao_social", "%" + dto.Razao_Social + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);
            List<DTO_Fornecedor> fora = new List<DTO_Fornecedor>();

            while (reader.Read())
            {
                DTO_Fornecedor dentro = new DTO_Fornecedor();
                dentro.ID = reader.GetInt32("id_fornecedor");
                dentro.Razao_Social = reader.GetString("ds_razao_social");
                dentro.CNPJ = reader.GetString("ds_cnpj");
                dentro.Telefone = reader.GetString("ds_telefone");
                dentro.Email = reader.GetString("ds_email");
                dentro.Endereco = reader.GetString("ds_endereco");
                dentro.CEP = reader.GetString("ds_cep");
                dentro.Complemento = reader.GetString("ds_complemento");
                dentro.Numero_Casa = reader.GetDecimal("nr_casa");
                dentro.UF = reader.GetString("ds_uf");
                dentro.Cidade = reader.GetString("ds_cidade");

                fora.Add(dentro);
            }

            reader.Close();
            return fora;
        }
    
        public bool VerificarFornecedor(DTO_Fornecedor dto)
        {
            string script = @"SELECT * FROM tb_fornecedor
                                     WHEHRE ds_cnpj = @ds_cnpj";

            List<MySqlParameter> parm = new List<MySqlParameter>();
            parm.Add(new MySqlParameter("ds_cnpj", dto.CNPJ));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parm);

            if (reader.Read())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
