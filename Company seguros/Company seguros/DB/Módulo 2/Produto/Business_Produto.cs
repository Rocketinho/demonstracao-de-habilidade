﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.MODULO2.Produto
{
    public class Business_Produto
    {
        //Variável de escopo
        Database_Produto db = new Database_Produto();

        //Método de Salvar
        public int Salvar(DTO_Produto dto)
        {
            return db.Salvar(dto);
        }

        //Método de Remover
        public void Remover(int id)
        {
            db.Remover(id);
        }

        //Método de Alterar
        public void Alterar(DTO_Produto dto)
        {
            db.Alterar(dto);
        }

        //Método de Listar (Sem filtro)
        public List<DTO_Produto> Listar()
        {
            List<DTO_Produto> list = db.Listar();
            return list;
        }

        //Método de Consultar (Com filtro)
        public List<DTO_Produto> Consultar(DTO_Produto dto)
        {
            List<DTO_Produto> list = db.Consultar(dto);
            return list;
        }
    }
}
