﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Cliente
{
    public class Business_Cliente
    {
        Database_Cliente db = new Database_Cliente();

        public int Salvar(DTO_Cliente dto)
        {
            // Nome Nulo
            if (dto.Nome.Trim() == string.Empty)
            {
                throw new ArgumentException("O nome não pode ser nulo");
            }
            else
            {
                if (dto.RG == null)
                {
                    throw new ArgumentException("O RG não pode ser nulo");

                }
                else
                {
                    if (dto.CPF == null)
                    {
                        throw new ArgumentException("O CPF não pode ser nulo");

                    }
                    else
                    {
                        if (dto.Sexo == null)
                        {
                            throw new ArgumentException("O campo Sexo não pode ser nulo");

                        }
                        else
                        {
                            
                                Validacao.Validacao validacao = new Validacao.Validacao();
                                string resp = validacao.ConsultarCPFBanco(dto.CPF);
                                if (resp == dto.CPF)
                                {
                                    throw new ArgumentException("Esse CPF já está cadastrado");

                                }
                                else
                                {
                                            return db.Salvar(dto);


                                        }
                                    }
                                }


                            }

                        }
                    }
                


            



        
        public void Alterar(DTO_Cliente dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Cliente dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Cliente> Listar()
        {
            return db.Listar();
        }
        public List<DTO_Cliente> Consultar(DTO_Cliente dto)
        {
            return db.Consultar(dto);
        }
    }
}
