﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Cliente
{
    public class DTO_Cliente
    {

        private string rg;
        public string RG
        {
            get
            {
                return rg;
            }
            set
            {
                rg = value;
            }
        }


        public int Id { get; set; }

        public string Nome { get; set; }

        public string CNPJ { get; set; }

        public string CPF { get; set; }

        //public string RG { get; set; }

        public string Sexo { get; set; }

        public DateTime  Nascimento{ get; set; }

        public string Endereco { get; set; }

        public string Cidade { get; set; }

        public string Complemento { get; set; }

        public string UF { get; set; }

        public string Numero { get; set; }

        public string Email{ get; set; }

        public string Telefone { get; set; }

    }
}
