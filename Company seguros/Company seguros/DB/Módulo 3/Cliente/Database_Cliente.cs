﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Cliente
{
    public class Database_Cliente
    {

        _base.Database db = new _base.Database();

        public int Salvar(DTO_Cliente dto)
        {
            string script =
                @"INSERT INTO tb_cliente
                (
                nm_cliente,
                ds_CNPJ,
                ds_CPF,
                ds_RG,
                ds_sexo,
                dt_nascimento,
                ds_endereco,
                ds_numero,
                ds_email, 
                ds_telefone,
                ds_complemento,
                ds_UF,
                ds_cidade
                )
                VALUES
                (
                @nm_cliente,
                @ds_CNPJ,
                @ds_CPF,
                @ds_RG,
                @ds_sexo,
                @dt_nascimento,
                @ds_endereco,    
                @ds_numero,              
                @ds_email, 
                @ds_telefone,
                @ds_complemento,
                @ds_UF,
                @ds_cidade
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_UF", dto.UF));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));

            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public void Alterar(DTO_Cliente dto)
        {
            string script =
               @"UPDATE tb_cliente SET nm_cliente = @nm_cliente,
                                       ds_CNPJ = @ds_CNPJ,
                                       ds_CPF = @ds_CPF,
                                       ds_RG = @ds_RG,
                                       ds_sexo = @ds_sexo,
                                       dt_nascimento = @dt_nascimento,
                                       ds_endereco = @ds_endereco,
                                       ds_numero = @ds_numero,
                                       ds_complemento = @ds_complemento,
                                       ds_email = @ds_email,
                                       ds_telefone = @ds_telefone,
                                       ds_cidade = @ds_cidade,
                                       ds_UF = @ds_UF
                WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_CNPJ", dto.CNPJ));
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("ds_RG", dto.RG));
            parms.Add(new MySqlParameter("ds_sexo", dto.Sexo));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));
            parms.Add(new MySqlParameter("ds_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("ds_numero", dto.Numero));
            parms.Add(new MySqlParameter("ds_complemento", dto.Complemento));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("ds_cidade", dto.Cidade));
            parms.Add(new MySqlParameter("ds_UF", dto.UF));



            db.ExecuteInsertScript(script, parms);
        }

        public void Remover(DTO_Cliente dto)
        {
            string script =
               @"DELETE * FROM tb_cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", dto.Id));          
            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_Cliente> Listar()
        {
            string script =
              @"SELECT * FROM tb_cliente";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_Cliente> lista = new List<DTO_Cliente>();

            while(reader.Read())
            {
                DTO_Cliente dt = new DTO_Cliente();
                dt.Id = reader.GetInt32("id_cliente");
                dt.Nome = reader.GetString("nm_cliente");
                dt.CNPJ = reader.GetString("ds_CNPJ");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Sexo = reader.GetString("ds_sexo");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Endereco = reader.GetString("ds_endereco");
                dt.Numero = reader.GetString("ds_numero");
                dt.Cidade = reader.GetString("ds_cidade");
                dt.Complemento = reader.GetString("ds_complemento");
                dt.UF = reader.GetString("ds_UF");
                dt.Email = reader.GetString("ds_email");
                dt.Telefone = reader.GetString("ds_telefone");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
        public List<DTO_Cliente> Consultar(DTO_Cliente dto)
        {
            string script =
              @"SELECT * FROM tb_cliente WHERE ds_CPF = @ds_CPF OR nm_cliente = @nm_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_CPF", dto.CPF));
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_Cliente> lista = new List<DTO_Cliente>();
            while (reader.Read())
            {
                DTO_Cliente dt = new DTO_Cliente();
                dt.Id = reader.GetInt32("id_cliente");
                dt.Nome = reader.GetString("nm_cliente");
                dt.CNPJ = reader.GetString("ds_CNPJ");
                dt.CPF = reader.GetString("ds_CPF");
                dt.RG = reader.GetString("ds_RG");
                dt.Sexo = reader.GetString("ds_sexo");
                dt.Nascimento = reader.GetDateTime("dt_nascimento");
                dt.Endereco = reader.GetString("ds_endereco");
                dt.Numero = reader.GetString("ds_numero");
                dt.Cidade = reader.GetString("ds_cidade");
                dt.Complemento = reader.GetString("ds_complemento");
                dt.UF = reader.GetString("ds_UF");
                dt.Email = reader.GetString("ds_email");
                dt.Telefone = reader.GetString("ds_telefone");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }



    }
}
