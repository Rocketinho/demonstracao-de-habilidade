﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.ProdutoVenda
{
    public class DTO_ProdutoVenda
    {
        public int ID { get; set; }

        public string Produto { get; set; }

        public decimal Preço { get; set; }

        public string Foto { get; set; }
    }
}
