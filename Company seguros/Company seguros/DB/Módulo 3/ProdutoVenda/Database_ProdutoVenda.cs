﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.ProdutoVenda
{
    public class Database_ProdutoVenda
    {
        _base.Database db = new _base.Database();

        public int Salvar(DTO_ProdutoVenda dto)
        {
            string script =
                @"INSERT INTO tb_produto_venda
                (
                nm_produto,
                vl_preco,
                img_foto
                )

                VALUES
                (
                @nm_produto,
                @vl_preco,
                @img_foto
                )";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preço));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));

            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        
        public void Alterar(DTO_ProdutoVenda dto)
        {
            string script =
               @"UPDATE tb_produto_venda SET nm_produto = @nm_produto,
                                             vl_preco = @vl_preco,
                                             img_foto = @img_foto
                WHERE id_produto_venda = @id_produto_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto_venda", dto.ID));
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preço));
            parms.Add(new MySqlParameter("img_foto", dto.Foto));

            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(DTO_ProdutoVenda dto)
        {
            string script =
               @"DELETE * FROM tb_produto_venda WHERE id_produto_venda = @id_produto_venda";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_produto_venda", dto.ID));
            

            db.ExecuteInsertScript(script, parms);
        }

        public List<DTO_ProdutoVenda> Listar()
        {
            string script =
                @"SELECT * FROM tb_produto_venda";

            MySqlDataReader reader = db.ExecuteSelectScript(script, null);
            List<DTO_ProdutoVenda> lista = new List<DTO_ProdutoVenda>();
            while(reader.Read())
            {
                DTO_ProdutoVenda dt = new DTO_ProdutoVenda();
                dt.ID = reader.GetInt32("id_produto_venda");
                dt.Produto = reader.GetString("nm_produto");
                dt.Preço = reader.GetDecimal("vl_preco");
                dt.Foto = reader.GetString("img_foto");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }

        public List<DTO_ProdutoVenda> Consultar(DTO_ProdutoVenda dto)
        {
            string script =
                @"SELECT * FROM tb_produto_venda WHERE nm_produto = @nm_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();          
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));

            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            List<DTO_ProdutoVenda> lista = new List<DTO_ProdutoVenda>();
            while (reader.Read())
            {
                DTO_ProdutoVenda dt = new DTO_ProdutoVenda();
                dt.ID = reader.GetInt32("id_produto_venda");
                dt.Produto = reader.GetString("nm_produto");
                dt.Preço = reader.GetDecimal("vl_preco");
                dt.Foto = reader.GetString("img_foto");
                lista.Add(dt);
            }
            reader.Close();
            return lista;
        }
    }
}
