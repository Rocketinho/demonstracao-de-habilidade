﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda
{
    public class Business_Venda
    {
        Database_Venda db = new Database_Venda();

        public int Salvar(DTO_Venda dto)
        {
            return Salvar(dto);
        }
        public void Alterar(DTO_Venda dto)
        {
            db.Alterar(dto);
        }
        public void Remover(DTO_Venda dto)
        {
            db.Remover(dto);
        }
        public List<DTO_Venda> Listar()
        {
            return db.Listar();
        }
        public List<DTO_Venda> Consultar(DTO_Venda dto)
        {
           return db.Consultar(dto);
        }
    }
}
