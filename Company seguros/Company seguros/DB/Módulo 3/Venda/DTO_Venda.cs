﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.DB.Módulo_3.Venda
{
    public class DTO_Venda
    {
        public int Id { get; set; }

        public int IdFuncionario { get; set; }

        public int IdCliente { get; set; }

        public int IdProduto { get; set; }

        public DateTime DataVenda { get; set; }
    }
}
