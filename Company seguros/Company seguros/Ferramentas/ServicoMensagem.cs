﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace Company_seguros
{
    static class ServicoMensagem
    {

        public static void EnviarSucesso(this Form form, string mensagem)
        {
            System.Windows.Forms.MessageBox.Show("SEGUROS", mensagem, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static void EnviarErro(this Form form, string mensagem)
        {
            System.Windows.Forms.MessageBox.Show("SEGUROS", mensagem, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        public static int Dobrar(this int a)
        {
            return a * 2;
        }

        
        public static void HandleError(this object a, Action funcao)
        {
            try
            {
                Task.Factory.StartNew(() =>
                {
                    funcao();
                });
            }
            catch (Exception ex)
            {
                EnviarErro(null, ex.Message);
            }
        }

        



    }
}
