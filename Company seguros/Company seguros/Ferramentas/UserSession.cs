﻿using Company_seguros.DB.Funcionario;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Company_seguros.DB.Funcionario.VIEW_Acesso;

namespace Company_seguros.Ferramentas
{
    public class UserSession
    {
        public static VIEW_Acesso_Funcionario Logado { get; set; }
    }
}
