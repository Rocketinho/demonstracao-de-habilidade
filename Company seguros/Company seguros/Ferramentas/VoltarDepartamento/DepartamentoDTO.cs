﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Company_seguros.Ferramentas.VoltarDepartamento
{
    public class DepartamentoDTO
    {
        public int ID_Departamento { get; set; }

        public string Departamento { get; set; }
    }
}
