﻿using Company_seguros.DB._base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Company_seguros.Validacao
{
    public class Validacao
    {
        public bool VerificarRG(string rg)
        {

            // Multiplicar os numeros de acordo com a sequencia de 2 a 9
            // Somar os valores que foram multiplicados
            // Pegar o RESTO da SOMA por 11, para assim, verificar se o RG é válido.
            try
            {
                // Pegando os 8 primeiros digitos digitalizados pelo usuário
                int n1 = int.Parse(rg.Substring(0, 1));
                int n2 = int.Parse(rg.Substring(1, 1));
                int n3 = int.Parse(rg.Substring(3, 1));
                int n4 = int.Parse(rg.Substring(4, 1));
                int n5 = int.Parse(rg.Substring(5, 1));
                int n6 = int.Parse(rg.Substring(7, 1));
                int n7 = int.Parse(rg.Substring(8, 1));
                int n8 = int.Parse(rg.Substring(9, 1));

                // Para verificar o digito verificador
                string dv = rg.Substring(11, 1);

                //Primeiro multiplique os valores acima em uma sequência de 2 à 9
                //Depois some esse valor.
                int soma = n1 * 2 + n2 * 3 + n3 * 4 + n4 * 5 + n5 * 6 + n6 * 7 + n7 * 8 + n8 * 9;

                //Pegue o valor total daquela soma e divida por 11 pegando apenas o RESTO da divisão
                string digitoverificador = Convert.ToString(soma % 11);

                //Se o digito Verificador for igual a 1, automagicamente o mesmo se tornará o algarismo Romano X...
                //Pois será efetuado o cálculo, no caso, 11 - 1(Digito verificador), dando o valor de 10. 

                if (digitoverificador == "1")
                {
                    digitoverificador = "X";
                }

                //Se o digito verificador for igual a 0, automagicamente o mesmo se tornará 0.
                //Pois 11 - 0 (Digito verificador) é igual a 11, sendo que o digito 11 não é permitido
                else if (digitoverificador == "0")
                {
                    digitoverificador = "0";
                }
                // Se não for nenhuma das condições feita acima, ele irá fazer o cálculo 11 - Digito Verificador
                else
                {
                    digitoverificador = (11 - int.Parse(digitoverificador)).ToString();
                }

                // Verificar se o dígito é igual os digitado pelo usuário
                if (dv == digitoverificador)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool VerificarCPF(string cpf)
        {
            try
            {
                if (cpf.Length == 14)
                {
                    int n1 = int.Parse(cpf.Substring(0, 1));
                    int n2 = int.Parse(cpf.Substring(1, 1));
                    int n3 = int.Parse(cpf.Substring(2, 1));
                    int n4 = int.Parse(cpf.Substring(4, 1));
                    int n5 = int.Parse(cpf.Substring(5, 1));
                    int n6 = int.Parse(cpf.Substring(6, 1));
                    int n7 = int.Parse(cpf.Substring(8, 1));
                    int n8 = int.Parse(cpf.Substring(9, 1));
                    int n9 = int.Parse(cpf.Substring(10, 1));

                    int digito1 = int.Parse(cpf.Substring(12, 1));
                    int digito2 = int.Parse(cpf.Substring(13, 1));

                    //Está verificando se os números são iguais. Se forem, o número colocado é burlado
                    if (n1 == n2 && n2 == n3 && n3 == n4 && n4 == n5 && n5 == n6 && n6 == n7 && n7 == n8 && n8 == n9)
                    {
                        return false;
                    }

                    // Aqui está sendo multiplicado e somado todos os números que o usuário digitou em uma sequência de 10 à 2.
                    int soma1 = n1 * 10 + n2 * 9 + n3 * 8 + n4 * 7 + n5 * 6 + n6 * 5 + n7 * 4 + n8 * 3 + n9 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador1 = soma1 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    // Agora, basicamente, é feito o mesmo processo...
                    // Soma e multiplicar todos os números em uma sequência de agora 11 números
                    int soma2 = n1 * 11 + n2 * 10 + n3 * 9 + n4 * 8 + n5 * 7 + n6 * 6 + n7 * 5 + n8 * 4 + n9 * 3 + digitoverificador1 * 2;

                    // Logo após, é pego o total da soma e é dividido por 11 pegando o RESTO dessa divisão. 
                    int digitoverificador2 = soma2 % 11;

                    // Verificar se o valor é menor ou maior que 2
                    if (digitoverificador2 < 2)
                    {
                        digitoverificador2 = 0;
                    }
                    else
                    {
                        digitoverificador2 = 11 - digitoverificador2;
                    }

                    // Verificar se os dois dígitos são iguais as digitados pelo usuário
                    if (digito1 == digitoverificador1 && digito2 == digitoverificador2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }
        public bool VerificarCNPJ(string cnpj)
        {
            // Primeiro deve-se colocar os 12 primeiros números em variáveis do tipo INTEIRO
            // Segundo Calcular esses 12 números e "gerar" os 2 digitos verificadores
            // E por fim, verificar se o CNPJ é verdadeiro ou falso.

            try
            {
                if (!(cnpj.Length < 18))
                {
                    int n1 = int.Parse(cnpj.Substring(0, 1));
                    int n2 = int.Parse(cnpj.Substring(1, 1));
                    int n3 = int.Parse(cnpj.Substring(3, 1));
                    int n4 = int.Parse(cnpj.Substring(4, 1));
                    int n5 = int.Parse(cnpj.Substring(5, 1));
                    int n6 = int.Parse(cnpj.Substring(7, 1));
                    int n7 = int.Parse(cnpj.Substring(8, 1));
                    int n8 = int.Parse(cnpj.Substring(9, 1));
                    int n9 = int.Parse(cnpj.Substring(11, 1));
                    int n10 = int.Parse(cnpj.Substring(12, 1));
                    int n11 = int.Parse(cnpj.Substring(13, 1));
                    int n12 = int.Parse(cnpj.Substring(14, 1));

                    int digito1 = int.Parse(cnpj.Substring(16, 1));
                    int digito2 = int.Parse(cnpj.Substring(17, 1));

                    if (n1 == 0 && n2 == 0 && n3 == 0 && n4 == 0 && n5 == 0 && n6 == 0 && n7 == 0 && n8 == 0 && n9 == 0 && n10 == 0 && n11 == 0 && n12 == 0)
                    {
                        return false;
                    }

                    int soma1 = n1 * 5 + n2 * 4 + n3 * 3 + n4 * 2 + n5 * 9 + n6 * 8 + n7 * 7 + n8 * 6 + n9 * 5 + n10 * 4 + n11 * 3 + n12 * 2;
                    int digitoverificador1 = soma1 % 11;

                    if (digitoverificador1 < 2)
                    {
                        digitoverificador1 = 0;
                    }
                    else
                    {
                        digitoverificador1 = 11 - digitoverificador1;
                    }

                    int soma2 = n1 * 6 + n2 * 5 + n3 * 4 + n4 * 3 + n5 * 2 + n6 * 9 + n7 * 8 + n8 * 7 + n9 * 6 + n10 * 5 + n11 * 4 + n12 * 3 + digitoverificador1 * 2;
                    int digitoverificador2 = soma2 % 11;

                    if (digitoverificador2 < 2)
                    {
                        digitoverificador2 = 0;
                    }
                    else
                    {
                        digitoverificador2 = 11 - digitoverificador2;
                    }

                    if (digito1 == digitoverificador1 && digito2 == digitoverificador2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                return false;
            }
        }

        //Verifica se há algum número
        public bool contemLetras(string texto)
        {
            if (texto.Where(c => char.IsLetter(c)).Count() > 0)
            {
                return true;
            }
            else
            {
                throw new ArgumentException("O campo nome não aceita valores númericos");
            }

        }
        //Verifica se há algum tipo de caractere
        public bool contemNumeros(string texto)
        {
            if (texto.Where(c => char.IsNumber(c)).Count() > 0)
            {
                return true;
            }
            else
            {
                throw new ArgumentException("O campo não aceita valores em caracteres");
            }



        }

        public bool VerificarNome(string nome)
        {
            /* [A-Z] --> Obrigatoriamente a primeira letra tem que ser maiuscula.
             * 
             * [a-zà-ú] ---> minuscula com ou sem acento 
             * 
             * {2,} --- > tem que aparecer no minimo duas vezes o trecho anterior a essa parte 
             * 
             * [a-zà-ú]{2,} ----> O trecho [a-zà-ú] tem que se repetir no minimo duas vezes, ou seja tem que se digitar duas
             *                    minusculas após a primeira maiuscula 
             *                    
             * [A-Z][a-zà-ú]{2,} ---> A primeira palavra digitada tem que ter a primeira letra maiuscula seguidada de
             *                        no minimo duas minusculas ou com ascento ou sem ascento que tem que se repetir no minimo 
             *                        duas vezes.
             *                        
             * \s? ---> pode ter um espaço ou não após o ultimo caractere que foi digitado.
             * 
             * \s?[A-Z][a-zà-ú']?{2,}? ---> é obrigatorio que tenha um segundo nome 
             * 
             * [ ?]---> está agrupando como opicional o trecho (\s?[A-Z][a-zà-ú]?{2,}?)
             * 
             * [A-Z][a-zà-ú']? ---> assim como o anterior porém é opicional e possui o ' porque alguns nomes tem 
             * 
             * [\s?[A-Z][a-zà-ú']?{2,}?] ---> é opicional e pode ou não repetir
             * 
             * [\s?[A-Z][a-zà-ú']?{2,}?]{1,} ---> pode repetir toda a parte dentro do [] uma ou mais vezes, mas por toda a parte 
             *                                    não ser obrigatoria dentro, não é obrigatorio
             */
            string ex = @"^[A-Z][a-zà-ú]{2,}\s?[A-Z][a-zà-ú']?{2,}?[\s?[A-Z][a-zà-ú']?{2,}?]{1,}$";
            Regex re = new Regex(ex);

            if (re.IsMatch(nome) == true) { return true; }
            else { return false; }

        }

        public bool Senha(string senha)
        {
            /*
             * * ?= ---> tem que ter
             * .* ---> qualquer um antes ou depois , mas tem que ter a parte que está obrigatoria([a-z])
             * (?=.*[a-z]) ---> tem que ter em algum lugar
             * 
             * [#$^+=!*()@%&] é a mesma coisa de [[:punct:]] 
             * .{8,16} ---> qualquer coisa antes , mas tem que ter de 8 á 16
             */

            string ex = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[[:punct:]]).{8,14}$";
            Regex re = new Regex(ex);
            if (re.IsMatch(senha) == true)
            {
                return true;
            }
            else
            {
                return true;
            }

        }

        public bool Placa(string value)
        {
            Regex regex = new Regex(@"^[a-zA-Z]{3}\-\d{4}$");

            if (regex.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void VerificarSexo(string sexo)
        {
            if (sexo == null)
            {
                throw new ArgumentException("O Sexo não pode ser nulo");
            }

        }
        public string ConsultarCPFBanco(string cpf)
        {
            string script =
                @"SELECT * FROM tb_funcionario WHERE ds_CPF = @ds_CPF";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_CPF", cpf));
           Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);
            string teste = null;
            while (reader.Read())
            {
                cpf = reader.GetString("ds_CPF");
                teste = cpf;
            }
            reader.Close();
            return teste;

        }
    }
}
