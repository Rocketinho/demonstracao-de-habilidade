﻿using Company_seguros.Ferramentas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Company_seguros.DB.Funcionario.VIEW_Acesso;

namespace Company_seguros
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnEntrar_Click(object sender, EventArgs e)
        {
            VIEW_Acesso_Funcionario dto = new VIEW_Acesso_Funcionario();
            dto.Usuario = txtUsuario.Text;
            dto.Senha = txtSenha.Text;

            VIEW_Acesso_Funcionario db = new VIEW_Acesso_Funcionario();
            VIEW_Acesso_Funcionario user = db.Logar(dto);

            if (user != null)
            {
                UserSession.Logado = user;
                this.Hide();
                frmMenu tela = new frmMenu();
                tela.Show();
            }
            else
            {
                MessageBox.Show("Credênciais inválidas!!",
                                "Company Seguros",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}
