﻿namespace Company_seguros
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuUsuario = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalvarU = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuBuscarU = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCliente = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuSalvarC = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuAlterarC = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarC = new System.Windows.Forms.ToolStripMenuItem();
            this.menuVenda = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuNova = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuVendaV = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuProdutosV = new System.Windows.Forms.ToolStripMenuItem();
            this.menuCOmpra = new System.Windows.Forms.ToolStripMenuItem();
            this.novoProdutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuCadastrarCo = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarCo = new System.Windows.Forms.ToolStripMenuItem();
            this.menuEstoque = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFornecedor = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuCadastrarF = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarF = new System.Windows.Forms.ToolStripMenuItem();
            this.menuFinanceiro = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuConsultarFC = new System.Windows.Forms.ToolStripMenuItem();
            this.menuRH = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuBaterR = new System.Windows.Forms.ToolStripMenuItem();
            this.subMenuFolha = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackColor = System.Drawing.Color.LightGray;
            this.statusStrip1.Location = new System.Drawing.Point(0, 299);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(544, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.LightGray;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuUsuario,
            this.menuCliente,
            this.menuVenda,
            this.menuCOmpra,
            this.menuEstoque,
            this.menuFornecedor,
            this.menuFinanceiro,
            this.menuRH});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(544, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuUsuario
            // 
            this.menuUsuario.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalvarU,
            this.subMenuBuscarU});
            this.menuUsuario.Name = "menuUsuario";
            this.menuUsuario.Size = new System.Drawing.Size(59, 20);
            this.menuUsuario.Text = "Usuário";
            // 
            // subMenuSalvarU
            // 
            this.subMenuSalvarU.Name = "subMenuSalvarU";
            this.subMenuSalvarU.Size = new System.Drawing.Size(124, 22);
            this.subMenuSalvarU.Text = "Cadastrar";
            this.subMenuSalvarU.Click += new System.EventHandler(this.subMenuSalvarU_Click);
            // 
            // subMenuBuscarU
            // 
            this.subMenuBuscarU.Name = "subMenuBuscarU";
            this.subMenuBuscarU.Size = new System.Drawing.Size(124, 22);
            this.subMenuBuscarU.Text = "Buscar";
            this.subMenuBuscarU.Click += new System.EventHandler(this.subMenuBuscarU_Click);
            // 
            // menuCliente
            // 
            this.menuCliente.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuSalvarC,
            this.subMenuAlterarC,
            this.subMenuConsultarC});
            this.menuCliente.Name = "menuCliente";
            this.menuCliente.Size = new System.Drawing.Size(56, 20);
            this.menuCliente.Text = "Cliente";
            // 
            // subMenuSalvarC
            // 
            this.subMenuSalvarC.Name = "subMenuSalvarC";
            this.subMenuSalvarC.Size = new System.Drawing.Size(152, 22);
            this.subMenuSalvarC.Text = "Cadastrar";
            this.subMenuSalvarC.Click += new System.EventHandler(this.cadrastrarToolStripMenuItem_Click);
            // 
            // subMenuAlterarC
            // 
            this.subMenuAlterarC.Name = "subMenuAlterarC";
            this.subMenuAlterarC.Size = new System.Drawing.Size(152, 22);
            this.subMenuAlterarC.Text = "Alterar";
            this.subMenuAlterarC.Click += new System.EventHandler(this.alterarToolStripMenuItem_Click);
            // 
            // subMenuConsultarC
            // 
            this.subMenuConsultarC.Name = "subMenuConsultarC";
            this.subMenuConsultarC.Size = new System.Drawing.Size(152, 22);
            this.subMenuConsultarC.Text = "Buscar";
            this.subMenuConsultarC.Click += new System.EventHandler(this.buscarToolStripMenuItem_Click);
            // 
            // menuVenda
            // 
            this.menuVenda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuNova,
            this.subMenuVendaV,
            this.subMenuProdutosV});
            this.menuVenda.Name = "menuVenda";
            this.menuVenda.Size = new System.Drawing.Size(52, 20);
            this.menuVenda.Text = "Venda";
            // 
            // subMenuNova
            // 
            this.subMenuNova.Name = "subMenuNova";
            this.subMenuNova.Size = new System.Drawing.Size(166, 22);
            this.subMenuNova.Text = "Nova Venda";
            this.subMenuNova.Click += new System.EventHandler(this.novaVendaToolStripMenuItem_Click);
            // 
            // subMenuVendaV
            // 
            this.subMenuVendaV.Name = "subMenuVendaV";
            this.subMenuVendaV.Size = new System.Drawing.Size(166, 22);
            this.subMenuVendaV.Text = "Consultar vendas";
            this.subMenuVendaV.Click += new System.EventHandler(this.consultarVendasToolStripMenuItem_Click);
            // 
            // subMenuProdutosV
            // 
            this.subMenuProdutosV.Name = "subMenuProdutosV";
            this.subMenuProdutosV.Size = new System.Drawing.Size(166, 22);
            this.subMenuProdutosV.Text = "Produtos a venda";
            this.subMenuProdutosV.Click += new System.EventHandler(this.produtosParaVendaToolStripMenuItem_Click);
            // 
            // menuCOmpra
            // 
            this.menuCOmpra.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.novoProdutoToolStripMenuItem,
            this.subMenuCadastrarCo,
            this.subMenuConsultarCo});
            this.menuCOmpra.Name = "menuCOmpra";
            this.menuCOmpra.Size = new System.Drawing.Size(62, 20);
            this.menuCOmpra.Text = "Compra";
            this.menuCOmpra.Click += new System.EventHandler(this.toolStripMenuItem9_Click);
            // 
            // novoProdutoToolStripMenuItem
            // 
            this.novoProdutoToolStripMenuItem.Name = "novoProdutoToolStripMenuItem";
            this.novoProdutoToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
            this.novoProdutoToolStripMenuItem.Text = "Novo Produto";
            this.novoProdutoToolStripMenuItem.Click += new System.EventHandler(this.novoProdutoToolStripMenuItem_Click);
            // 
            // subMenuCadastrarCo
            // 
            this.subMenuCadastrarCo.Name = "subMenuCadastrarCo";
            this.subMenuCadastrarCo.Size = new System.Drawing.Size(171, 22);
            this.subMenuCadastrarCo.Text = "Nova Compra";
            this.subMenuCadastrarCo.Click += new System.EventHandler(this.toolStripMenuItem10_Click);
            // 
            // subMenuConsultarCo
            // 
            this.subMenuConsultarCo.Name = "subMenuConsultarCo";
            this.subMenuConsultarCo.Size = new System.Drawing.Size(171, 22);
            this.subMenuConsultarCo.Text = "Consultar Compra";
            this.subMenuConsultarCo.Click += new System.EventHandler(this.subMenuConsultarCo_Click);
            // 
            // menuEstoque
            // 
            this.menuEstoque.Name = "menuEstoque";
            this.menuEstoque.Size = new System.Drawing.Size(61, 20);
            this.menuEstoque.Text = "Estoque";
            this.menuEstoque.Click += new System.EventHandler(this.estoqueToolStripMenuItem_Click);
            // 
            // menuFornecedor
            // 
            this.menuFornecedor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuCadastrarF,
            this.subMenuConsultarF});
            this.menuFornecedor.Name = "menuFornecedor";
            this.menuFornecedor.Size = new System.Drawing.Size(79, 20);
            this.menuFornecedor.Text = "Fornecedor";
            // 
            // subMenuCadastrarF
            // 
            this.subMenuCadastrarF.Name = "subMenuCadastrarF";
            this.subMenuCadastrarF.Size = new System.Drawing.Size(124, 22);
            this.subMenuCadastrarF.Text = "Cadastrar";
            this.subMenuCadastrarF.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // subMenuConsultarF
            // 
            this.subMenuConsultarF.Name = "subMenuConsultarF";
            this.subMenuConsultarF.Size = new System.Drawing.Size(124, 22);
            this.subMenuConsultarF.Text = "Buscar";
            this.subMenuConsultarF.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // menuFinanceiro
            // 
            this.menuFinanceiro.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuConsultarFC});
            this.menuFinanceiro.Name = "menuFinanceiro";
            this.menuFinanceiro.Size = new System.Drawing.Size(74, 20);
            this.menuFinanceiro.Text = "Financeiro";
            // 
            // subMenuConsultarFC
            // 
            this.subMenuConsultarFC.Name = "subMenuConsultarFC";
            this.subMenuConsultarFC.Size = new System.Drawing.Size(149, 22);
            this.subMenuConsultarFC.Text = "Fluxo de Caixa";
            this.subMenuConsultarFC.Click += new System.EventHandler(this.produtoParaCopToolStripMenuItem_Click);
            // 
            // menuRH
            // 
            this.menuRH.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.subMenuBaterR,
            this.subMenuFolha});
            this.menuRH.Name = "menuRH";
            this.menuRH.Size = new System.Drawing.Size(35, 20);
            this.menuRH.Text = "RH";
            // 
            // subMenuBaterR
            // 
            this.subMenuBaterR.Name = "subMenuBaterR";
            this.subMenuBaterR.Size = new System.Drawing.Size(183, 22);
            this.subMenuBaterR.Text = "Bater Ponto";
            this.subMenuBaterR.Click += new System.EventHandler(this.baterPontoToolStripMenuItem_Click);
            // 
            // subMenuFolha
            // 
            this.subMenuFolha.Name = "subMenuFolha";
            this.subMenuFolha.Size = new System.Drawing.Size(183, 22);
            this.subMenuFolha.Text = "Folha de pagamento";
            this.subMenuFolha.Click += new System.EventHandler(this.folhaDePagamentoToolStripMenuItem_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(544, 321);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Menu";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuUsuario;
        private System.Windows.Forms.ToolStripMenuItem subMenuBuscarU;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalvarU;
        private System.Windows.Forms.ToolStripMenuItem menuCliente;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarC;
        private System.Windows.Forms.ToolStripMenuItem subMenuAlterarC;
        private System.Windows.Forms.ToolStripMenuItem subMenuSalvarC;
        private System.Windows.Forms.ToolStripMenuItem menuVenda;
        private System.Windows.Forms.ToolStripMenuItem subMenuNova;
        private System.Windows.Forms.ToolStripMenuItem subMenuVendaV;
        private System.Windows.Forms.ToolStripMenuItem menuEstoque;
        private System.Windows.Forms.ToolStripMenuItem menuFornecedor;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarF;
        private System.Windows.Forms.ToolStripMenuItem subMenuCadastrarF;
        private System.Windows.Forms.ToolStripMenuItem subMenuProdutosV;
        private System.Windows.Forms.ToolStripMenuItem menuCOmpra;
        private System.Windows.Forms.ToolStripMenuItem subMenuCadastrarCo;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarCo;
        private System.Windows.Forms.ToolStripMenuItem menuFinanceiro;
        private System.Windows.Forms.ToolStripMenuItem subMenuConsultarFC;
        private System.Windows.Forms.ToolStripMenuItem menuRH;
        private System.Windows.Forms.ToolStripMenuItem subMenuBaterR;
        private System.Windows.Forms.ToolStripMenuItem subMenuFolha;
        private System.Windows.Forms.ToolStripMenuItem novoProdutoToolStripMenuItem;
    }
}