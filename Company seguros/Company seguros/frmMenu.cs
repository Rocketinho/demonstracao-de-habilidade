﻿using Company_seguros.Ferramentas;
using Company_seguros.zTelas._1Usuario;
using Company_seguros.zTelas._2Cliente;
using Company_seguros.zTelas._3Venda;
using Company_seguros.zTelas._4Compra;
using Company_seguros.zTelas._5Estoque;
using Company_seguros.zTelas._6Fornecedor;
using Company_seguros.zTelas._8RH;
using Company_seguros.zTelas.Cliente;
using Company_seguros.zTelas.Fluxo_de_caixa;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            //CarregarPermissoes();
            //Atual = this;
        }
        public static frmMenu Atual;

       

        private void CarregarPermissoes ()
        {
            if (UserSession.Logado.Salvar == false)
            {
                subMenuSalvarU.Enabled = false;
                subMenuSalvarC.Enabled = false;
                subMenuNova.Enabled = false;    
                subMenuCadastrarCo.Enabled = false;    
                subMenuCadastrarF.Enabled = false;    
                subMenuBaterR.Enabled = false;    
            }
            if (UserSession.Logado.Consultar == false)
            {
                subMenuBuscarU.Enabled = false;
                subMenuConsultarC.Enabled = false;
                subMenuProdutosV.Enabled = false;
                subMenuVendaV.Enabled = false;
                subMenuConsultarCo.Enabled = false;
                //subMenuProdutosCo.Enabled = false;
                menuEstoque.Enabled = false;
                subMenuConsultarF.Enabled = false;
                subMenuConsultarFC.Enabled = false;
                subMenuFolha.Enabled = false;
            }
            if (UserSession.Logado.Alterar == false)
            {
             
                subMenuAlterarC.Enabled = false;    
                //subMenuAlterarF.Enabled = false;    
            }
        }

        private void cadrastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastrar_Cliente tela = new Cadastrar_Cliente();
            tela.Show();
            Hide();
        }

        private void alterarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Alterar_cliente tela = new Alterar_cliente();
            tela.Show();
            Hide();
        }

        private void buscarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BuscarCliente tela = new BuscarCliente();
            tela.Show();
            Hide();
        }

        private void novaVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Novavenda tela = new Novavenda();
            tela.Show();
            Hide();
        }

        private void consultarVendasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosVenda tela = new ProdutosVenda();
            tela.Show();
            Hide();
        }

        private void produtosParaVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarProdutoVenda tela = new CadastrarProdutoVenda();
            tela.Show();
            Hide();
        }

        private void toolStripMenuItem9_Click(object sender, EventArgs e)
        {

        }

        private void toolStripMenuItem10_Click(object sender, EventArgs e)
        {
            NovaCompra tela = new NovaCompra();
            tela.Show();
            Hide();
        }

        private void estoqueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarEstoque tela = new ConsultarEstoque();
            tela.Show();
            Hide();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            CadastrarFornecedor tela = new CadastrarFornecedor();
            tela.Show();
            Hide();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            AlterarFornecedor tela = new AlterarFornecedor();
            tela.Show();
            Hide();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            BuscarFornecedor tela = new BuscarFornecedor();
            tela.Show();
            Hide();
        }

        private void produtoParaCopToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Fluxo_de_Caixa tela = new Fluxo_de_Caixa();
            tela.Show();
            Hide();
        }

        private void baterPontoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BaterPonto tela = new BaterPonto();
            tela.Show();
            Hide(); 
        }

        private void folhaDePagamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolhaPagamento tela = new FolhaPagamento();
            tela.Show();
            Hide();
        }

        

        private void subMenuProdutosCo_Click(object sender, EventArgs e)
        {

        }

        private void subMenuSalvarU_Click(object sender, EventArgs e)
        {
            UsuarioCadastrar tel = new UsuarioCadastrar();
            tel.Show();
            Hide();
        }

        private void subMenuAlterarU_Click(object sender, EventArgs e)
        {
            UsuarioAlterar tela = new UsuarioAlterar();
            tela.Show();
            Hide();
        }

        private void subMenuBuscarU_Click(object sender, EventArgs e)
        {
            UsuarioBuscar tela = new UsuarioBuscar();
            tela.Show();
            Hide();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {

        }

        private void novoProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarProdutoCompra tela = new CadastrarProdutoCompra();
            tela.Show();
            Hide();
        }

        private void subMenuConsultarCo_Click(object sender, EventArgs e)
        {
            ProdutosCompra tela = new ProdutosCompra();
            tela.Show();
            Hide();
        }
    }
}
