﻿namespace Company_seguros.zTelas._1Usuario
{
    partial class UsuarioAlterar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.voltarAoMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.gpbEstoque = new System.Windows.Forms.GroupBox();
            this.chkConsultarEstoque = new System.Windows.Forms.CheckBox();
            this.chkSalvarEstoque = new System.Windows.Forms.CheckBox();
            this.chkAlterarEstoque = new System.Windows.Forms.CheckBox();
            this.chkRemoverEstoque = new System.Windows.Forms.CheckBox();
            this.gpbCompras = new System.Windows.Forms.GroupBox();
            this.chkConsultarCompras = new System.Windows.Forms.CheckBox();
            this.chkSalvarCompras = new System.Windows.Forms.CheckBox();
            this.chkAlterarCompras = new System.Windows.Forms.CheckBox();
            this.chkRemoverCompras = new System.Windows.Forms.CheckBox();
            this.gpbFinanceiro = new System.Windows.Forms.GroupBox();
            this.chkConsultarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkSalvarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkAlterarFinanceiro = new System.Windows.Forms.CheckBox();
            this.chkRemoverFinanceiro = new System.Windows.Forms.CheckBox();
            this.gpbRH = new System.Windows.Forms.GroupBox();
            this.chkConsultarRH = new System.Windows.Forms.CheckBox();
            this.chkSalvarRH = new System.Windows.Forms.CheckBox();
            this.chkAlterarRH = new System.Windows.Forms.CheckBox();
            this.chkRemoverRH = new System.Windows.Forms.CheckBox();
            this.gpbFuncionario = new System.Windows.Forms.GroupBox();
            this.chkConsultarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkSalvarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkAlterarFuncionario = new System.Windows.Forms.CheckBox();
            this.chkRemoverFuncionario = new System.Windows.Forms.CheckBox();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.gpbVendas = new System.Windows.Forms.GroupBox();
            this.chkConsultarVendas = new System.Windows.Forms.CheckBox();
            this.chkSalvarVendas = new System.Windows.Forms.CheckBox();
            this.chkAlterarVendas = new System.Windows.Forms.CheckBox();
            this.chkRemoverVendas = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.nudConvenio = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.nudSalario = new System.Windows.Forms.NumericUpDown();
            this.nudVT = new System.Windows.Forms.NumericUpDown();
            this.nudVR = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtSenha = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblSenha = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtRua = new System.Windows.Forms.TextBox();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpNascimento = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkF = new System.Windows.Forms.RadioButton();
            this.chkM = new System.Windows.Forms.RadioButton();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtCPF = new System.Windows.Forms.MaskedTextBox();
            this.txtRG = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.gpbEstoque.SuspendLayout();
            this.gpbCompras.SuspendLayout();
            this.gpbFinanceiro.SuspendLayout();
            this.gpbRH.SuspendLayout();
            this.gpbFuncionario.SuspendLayout();
            this.gpbVendas.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 383);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(550, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voltarAoMenuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(550, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // voltarAoMenuToolStripMenuItem
            // 
            this.voltarAoMenuToolStripMenuItem.Name = "voltarAoMenuToolStripMenuItem";
            this.voltarAoMenuToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.voltarAoMenuToolStripMenuItem.Text = "Voltar ao menu";
            this.voltarAoMenuToolStripMenuItem.Click += new System.EventHandler(this.voltarAoMenuToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(-2, 24);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(552, 363);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(544, 337);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Funcionários";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(544, 337);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Permissão";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.gpbEstoque);
            this.groupBox5.Controls.Add(this.gpbCompras);
            this.groupBox5.Controls.Add(this.gpbFinanceiro);
            this.groupBox5.Controls.Add(this.gpbRH);
            this.groupBox5.Controls.Add(this.gpbFuncionario);
            this.groupBox5.Controls.Add(this.btnAlterar);
            this.groupBox5.Controls.Add(this.gpbVendas);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.groupBox5.Location = new System.Drawing.Point(3, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(530, 328);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Permissão";
            this.groupBox5.Enter += new System.EventHandler(this.groupBox5_Enter);
            // 
            // gpbEstoque
            // 
            this.gpbEstoque.Controls.Add(this.chkConsultarEstoque);
            this.gpbEstoque.Controls.Add(this.chkSalvarEstoque);
            this.gpbEstoque.Controls.Add(this.chkAlterarEstoque);
            this.gpbEstoque.Controls.Add(this.chkRemoverEstoque);
            this.gpbEstoque.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbEstoque.Location = new System.Drawing.Point(349, 121);
            this.gpbEstoque.Name = "gpbEstoque";
            this.gpbEstoque.Size = new System.Drawing.Size(166, 97);
            this.gpbEstoque.TabIndex = 11;
            this.gpbEstoque.TabStop = false;
            this.gpbEstoque.Text = "Estoque";
            // 
            // chkConsultarEstoque
            // 
            this.chkConsultarEstoque.AutoSize = true;
            this.chkConsultarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarEstoque.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarEstoque.Name = "chkConsultarEstoque";
            this.chkConsultarEstoque.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarEstoque.TabIndex = 41;
            this.chkConsultarEstoque.Text = "Consultar";
            this.chkConsultarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkSalvarEstoque
            // 
            this.chkSalvarEstoque.AutoSize = true;
            this.chkSalvarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarEstoque.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarEstoque.Name = "chkSalvarEstoque";
            this.chkSalvarEstoque.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarEstoque.TabIndex = 42;
            this.chkSalvarEstoque.Text = "Salvar";
            this.chkSalvarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkAlterarEstoque
            // 
            this.chkAlterarEstoque.AutoSize = true;
            this.chkAlterarEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarEstoque.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarEstoque.Name = "chkAlterarEstoque";
            this.chkAlterarEstoque.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarEstoque.TabIndex = 43;
            this.chkAlterarEstoque.Text = "Alterar";
            this.chkAlterarEstoque.UseVisualStyleBackColor = true;
            // 
            // chkRemoverEstoque
            // 
            this.chkRemoverEstoque.AutoSize = true;
            this.chkRemoverEstoque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverEstoque.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverEstoque.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverEstoque.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverEstoque.Name = "chkRemoverEstoque";
            this.chkRemoverEstoque.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverEstoque.TabIndex = 44;
            this.chkRemoverEstoque.Text = "Remover";
            this.chkRemoverEstoque.UseVisualStyleBackColor = true;
            // 
            // gpbCompras
            // 
            this.gpbCompras.Controls.Add(this.chkConsultarCompras);
            this.gpbCompras.Controls.Add(this.chkSalvarCompras);
            this.gpbCompras.Controls.Add(this.chkAlterarCompras);
            this.gpbCompras.Controls.Add(this.chkRemoverCompras);
            this.gpbCompras.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbCompras.Location = new System.Drawing.Point(175, 121);
            this.gpbCompras.Name = "gpbCompras";
            this.gpbCompras.Size = new System.Drawing.Size(161, 97);
            this.gpbCompras.TabIndex = 10;
            this.gpbCompras.TabStop = false;
            this.gpbCompras.Text = "Compras";
            // 
            // chkConsultarCompras
            // 
            this.chkConsultarCompras.AutoSize = true;
            this.chkConsultarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarCompras.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarCompras.Name = "chkConsultarCompras";
            this.chkConsultarCompras.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarCompras.TabIndex = 41;
            this.chkConsultarCompras.Text = "Consultar";
            this.chkConsultarCompras.UseVisualStyleBackColor = true;
            // 
            // chkSalvarCompras
            // 
            this.chkSalvarCompras.AutoSize = true;
            this.chkSalvarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarCompras.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarCompras.Name = "chkSalvarCompras";
            this.chkSalvarCompras.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarCompras.TabIndex = 42;
            this.chkSalvarCompras.Text = "Salvar";
            this.chkSalvarCompras.UseVisualStyleBackColor = true;
            // 
            // chkAlterarCompras
            // 
            this.chkAlterarCompras.AutoSize = true;
            this.chkAlterarCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarCompras.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarCompras.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarCompras.Name = "chkAlterarCompras";
            this.chkAlterarCompras.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarCompras.TabIndex = 43;
            this.chkAlterarCompras.Text = "Alterar";
            this.chkAlterarCompras.UseVisualStyleBackColor = true;
            // 
            // chkRemoverCompras
            // 
            this.chkRemoverCompras.AutoSize = true;
            this.chkRemoverCompras.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverCompras.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverCompras.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverCompras.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverCompras.Name = "chkRemoverCompras";
            this.chkRemoverCompras.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverCompras.TabIndex = 44;
            this.chkRemoverCompras.Text = "Remover";
            this.chkRemoverCompras.UseVisualStyleBackColor = true;
            // 
            // gpbFinanceiro
            // 
            this.gpbFinanceiro.Controls.Add(this.chkConsultarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkSalvarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkAlterarFinanceiro);
            this.gpbFinanceiro.Controls.Add(this.chkRemoverFinanceiro);
            this.gpbFinanceiro.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFinanceiro.Location = new System.Drawing.Point(0, 121);
            this.gpbFinanceiro.Name = "gpbFinanceiro";
            this.gpbFinanceiro.Size = new System.Drawing.Size(164, 97);
            this.gpbFinanceiro.TabIndex = 9;
            this.gpbFinanceiro.TabStop = false;
            this.gpbFinanceiro.Text = "Financeiro";
            // 
            // chkConsultarFinanceiro
            // 
            this.chkConsultarFinanceiro.AutoSize = true;
            this.chkConsultarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFinanceiro.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFinanceiro.Name = "chkConsultarFinanceiro";
            this.chkConsultarFinanceiro.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFinanceiro.TabIndex = 41;
            this.chkConsultarFinanceiro.Text = "Consultar";
            this.chkConsultarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFinanceiro
            // 
            this.chkSalvarFinanceiro.AutoSize = true;
            this.chkSalvarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFinanceiro.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarFinanceiro.Name = "chkSalvarFinanceiro";
            this.chkSalvarFinanceiro.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFinanceiro.TabIndex = 42;
            this.chkSalvarFinanceiro.Text = "Salvar";
            this.chkSalvarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFinanceiro
            // 
            this.chkAlterarFinanceiro.AutoSize = true;
            this.chkAlterarFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFinanceiro.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarFinanceiro.Name = "chkAlterarFinanceiro";
            this.chkAlterarFinanceiro.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFinanceiro.TabIndex = 43;
            this.chkAlterarFinanceiro.Text = "Alterar";
            this.chkAlterarFinanceiro.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFinanceiro
            // 
            this.chkRemoverFinanceiro.AutoSize = true;
            this.chkRemoverFinanceiro.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFinanceiro.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFinanceiro.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFinanceiro.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverFinanceiro.Name = "chkRemoverFinanceiro";
            this.chkRemoverFinanceiro.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFinanceiro.TabIndex = 44;
            this.chkRemoverFinanceiro.Text = "Remover";
            this.chkRemoverFinanceiro.UseVisualStyleBackColor = true;
            // 
            // gpbRH
            // 
            this.gpbRH.Controls.Add(this.chkConsultarRH);
            this.gpbRH.Controls.Add(this.chkSalvarRH);
            this.gpbRH.Controls.Add(this.chkAlterarRH);
            this.gpbRH.Controls.Add(this.chkRemoverRH);
            this.gpbRH.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbRH.Location = new System.Drawing.Point(349, 22);
            this.gpbRH.Name = "gpbRH";
            this.gpbRH.Size = new System.Drawing.Size(166, 93);
            this.gpbRH.TabIndex = 8;
            this.gpbRH.TabStop = false;
            this.gpbRH.Text = "RH";
            // 
            // chkConsultarRH
            // 
            this.chkConsultarRH.AutoSize = true;
            this.chkConsultarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarRH.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarRH.Location = new System.Drawing.Point(5, 25);
            this.chkConsultarRH.Name = "chkConsultarRH";
            this.chkConsultarRH.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarRH.TabIndex = 41;
            this.chkConsultarRH.Text = "Consultar";
            this.chkConsultarRH.UseVisualStyleBackColor = true;
            // 
            // chkSalvarRH
            // 
            this.chkSalvarRH.AutoSize = true;
            this.chkSalvarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarRH.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarRH.Location = new System.Drawing.Point(91, 25);
            this.chkSalvarRH.Name = "chkSalvarRH";
            this.chkSalvarRH.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarRH.TabIndex = 42;
            this.chkSalvarRH.Text = "Salvar";
            this.chkSalvarRH.UseVisualStyleBackColor = true;
            // 
            // chkAlterarRH
            // 
            this.chkAlterarRH.AutoSize = true;
            this.chkAlterarRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarRH.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarRH.Location = new System.Drawing.Point(5, 54);
            this.chkAlterarRH.Name = "chkAlterarRH";
            this.chkAlterarRH.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarRH.TabIndex = 43;
            this.chkAlterarRH.Text = "Alterar";
            this.chkAlterarRH.UseVisualStyleBackColor = true;
            // 
            // chkRemoverRH
            // 
            this.chkRemoverRH.AutoSize = true;
            this.chkRemoverRH.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverRH.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverRH.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverRH.Location = new System.Drawing.Point(91, 54);
            this.chkRemoverRH.Name = "chkRemoverRH";
            this.chkRemoverRH.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverRH.TabIndex = 44;
            this.chkRemoverRH.Text = "Remover";
            this.chkRemoverRH.UseVisualStyleBackColor = true;
            // 
            // gpbFuncionario
            // 
            this.gpbFuncionario.Controls.Add(this.chkConsultarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkSalvarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkAlterarFuncionario);
            this.gpbFuncionario.Controls.Add(this.chkRemoverFuncionario);
            this.gpbFuncionario.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbFuncionario.Location = new System.Drawing.Point(175, 22);
            this.gpbFuncionario.Name = "gpbFuncionario";
            this.gpbFuncionario.Size = new System.Drawing.Size(161, 93);
            this.gpbFuncionario.TabIndex = 4;
            this.gpbFuncionario.TabStop = false;
            this.gpbFuncionario.Text = "Funcionario";
            // 
            // chkConsultarFuncionario
            // 
            this.chkConsultarFuncionario.AutoSize = true;
            this.chkConsultarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarFuncionario.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarFuncionario.Name = "chkConsultarFuncionario";
            this.chkConsultarFuncionario.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarFuncionario.TabIndex = 41;
            this.chkConsultarFuncionario.Text = "Consultar";
            this.chkConsultarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkSalvarFuncionario
            // 
            this.chkSalvarFuncionario.AutoSize = true;
            this.chkSalvarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarFuncionario.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarFuncionario.Name = "chkSalvarFuncionario";
            this.chkSalvarFuncionario.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarFuncionario.TabIndex = 42;
            this.chkSalvarFuncionario.Text = "Salvar";
            this.chkSalvarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkAlterarFuncionario
            // 
            this.chkAlterarFuncionario.AutoSize = true;
            this.chkAlterarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarFuncionario.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarFuncionario.Name = "chkAlterarFuncionario";
            this.chkAlterarFuncionario.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarFuncionario.TabIndex = 43;
            this.chkAlterarFuncionario.Text = "Alterar";
            this.chkAlterarFuncionario.UseVisualStyleBackColor = true;
            // 
            // chkRemoverFuncionario
            // 
            this.chkRemoverFuncionario.AutoSize = true;
            this.chkRemoverFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverFuncionario.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverFuncionario.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverFuncionario.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverFuncionario.Name = "chkRemoverFuncionario";
            this.chkRemoverFuncionario.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverFuncionario.TabIndex = 44;
            this.chkRemoverFuncionario.Text = "Remover";
            this.chkRemoverFuncionario.UseVisualStyleBackColor = true;
            // 
            // btnAlterar
            // 
            this.btnAlterar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.Location = new System.Drawing.Point(204, 237);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(91, 30);
            this.btnAlterar.TabIndex = 7;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = true;
            this.btnAlterar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // gpbVendas
            // 
            this.gpbVendas.Controls.Add(this.chkConsultarVendas);
            this.gpbVendas.Controls.Add(this.chkSalvarVendas);
            this.gpbVendas.Controls.Add(this.chkAlterarVendas);
            this.gpbVendas.Controls.Add(this.chkRemoverVendas);
            this.gpbVendas.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.gpbVendas.Location = new System.Drawing.Point(0, 22);
            this.gpbVendas.Name = "gpbVendas";
            this.gpbVendas.Size = new System.Drawing.Size(163, 93);
            this.gpbVendas.TabIndex = 0;
            this.gpbVendas.TabStop = false;
            this.gpbVendas.Text = "Vendas";
            // 
            // chkConsultarVendas
            // 
            this.chkConsultarVendas.AutoSize = true;
            this.chkConsultarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkConsultarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConsultarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkConsultarVendas.Location = new System.Drawing.Point(6, 25);
            this.chkConsultarVendas.Name = "chkConsultarVendas";
            this.chkConsultarVendas.Size = new System.Drawing.Size(76, 17);
            this.chkConsultarVendas.TabIndex = 41;
            this.chkConsultarVendas.Text = "Consultar";
            this.chkConsultarVendas.UseVisualStyleBackColor = true;
            // 
            // chkSalvarVendas
            // 
            this.chkSalvarVendas.AutoSize = true;
            this.chkSalvarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkSalvarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSalvarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkSalvarVendas.Location = new System.Drawing.Point(92, 25);
            this.chkSalvarVendas.Name = "chkSalvarVendas";
            this.chkSalvarVendas.Size = new System.Drawing.Size(57, 17);
            this.chkSalvarVendas.TabIndex = 42;
            this.chkSalvarVendas.Text = "Salvar";
            this.chkSalvarVendas.UseVisualStyleBackColor = true;
            // 
            // chkAlterarVendas
            // 
            this.chkAlterarVendas.AutoSize = true;
            this.chkAlterarVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkAlterarVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlterarVendas.ForeColor = System.Drawing.Color.Black;
            this.chkAlterarVendas.Location = new System.Drawing.Point(6, 54);
            this.chkAlterarVendas.Name = "chkAlterarVendas";
            this.chkAlterarVendas.Size = new System.Drawing.Size(61, 17);
            this.chkAlterarVendas.TabIndex = 43;
            this.chkAlterarVendas.Text = "Alterar";
            this.chkAlterarVendas.UseVisualStyleBackColor = true;
            // 
            // chkRemoverVendas
            // 
            this.chkRemoverVendas.AutoSize = true;
            this.chkRemoverVendas.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkRemoverVendas.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRemoverVendas.ForeColor = System.Drawing.Color.Black;
            this.chkRemoverVendas.Location = new System.Drawing.Point(92, 54);
            this.chkRemoverVendas.Name = "chkRemoverVendas";
            this.chkRemoverVendas.Size = new System.Drawing.Size(72, 17);
            this.chkRemoverVendas.TabIndex = 44;
            this.chkRemoverVendas.Text = "Remover";
            this.chkRemoverVendas.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox7);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.dtpNascimento);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.txtCPF);
            this.groupBox1.Controls.Add(this.txtRG);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(538, 312);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados do cliente";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label12);
            this.groupBox7.Controls.Add(this.nudConvenio);
            this.groupBox7.Controls.Add(this.label13);
            this.groupBox7.Controls.Add(this.label14);
            this.groupBox7.Controls.Add(this.nudVA);
            this.groupBox7.Controls.Add(this.nudSalario);
            this.groupBox7.Controls.Add(this.nudVT);
            this.groupBox7.Controls.Add(this.nudVR);
            this.groupBox7.Location = new System.Drawing.Point(312, 13);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(218, 172);
            this.groupBox7.TabIndex = 52;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = " ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(6, 17);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(49, 15);
            this.label15.TabIndex = 44;
            this.label15.Text = "Salário:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(6, 108);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 15);
            this.label16.TabIndex = 45;
            this.label16.Text = "VR:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(10, 73);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(24, 15);
            this.label12.TabIndex = 41;
            this.label12.Text = "VA:";
            // 
            // nudConvenio
            // 
            this.nudConvenio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudConvenio.DecimalPlaces = 2;
            this.nudConvenio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudConvenio.ForeColor = System.Drawing.Color.Black;
            this.nudConvenio.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudConvenio.Location = new System.Drawing.Point(72, 138);
            this.nudConvenio.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudConvenio.Name = "nudConvenio";
            this.nudConvenio.Size = new System.Drawing.Size(140, 20);
            this.nudConvenio.TabIndex = 46;
            this.nudConvenio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(8, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(24, 15);
            this.label13.TabIndex = 42;
            this.label13.Text = "VT:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(6, 140);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 15);
            this.label14.TabIndex = 43;
            this.label14.Text = "Convênio:";
            // 
            // nudVA
            // 
            this.nudVA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVA.ForeColor = System.Drawing.Color.Black;
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(72, 73);
            this.nudVA.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(140, 20);
            this.nudVA.TabIndex = 47;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudSalario
            // 
            this.nudSalario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudSalario.DecimalPlaces = 2;
            this.nudSalario.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudSalario.ForeColor = System.Drawing.Color.Black;
            this.nudSalario.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalario.Location = new System.Drawing.Point(72, 15);
            this.nudSalario.Maximum = new decimal(new int[] {
            30000,
            0,
            0,
            0});
            this.nudSalario.Name = "nudSalario";
            this.nudSalario.Size = new System.Drawing.Size(140, 20);
            this.nudSalario.TabIndex = 50;
            this.nudSalario.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVT
            // 
            this.nudVT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVT.DecimalPlaces = 2;
            this.nudVT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVT.ForeColor = System.Drawing.Color.Black;
            this.nudVT.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVT.Location = new System.Drawing.Point(72, 41);
            this.nudVT.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVT.Name = "nudVT";
            this.nudVT.Size = new System.Drawing.Size(140, 20);
            this.nudVT.TabIndex = 48;
            this.nudVT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudVR
            // 
            this.nudVR.Cursor = System.Windows.Forms.Cursors.Hand;
            this.nudVR.DecimalPlaces = 2;
            this.nudVR.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.nudVR.ForeColor = System.Drawing.Color.Black;
            this.nudVR.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVR.Location = new System.Drawing.Point(71, 108);
            this.nudVR.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.nudVR.Name = "nudVR";
            this.nudVR.Size = new System.Drawing.Size(140, 20);
            this.nudVR.TabIndex = 49;
            this.nudVR.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.txtUsuario);
            this.groupBox6.Controls.Add(this.txtSenha);
            this.groupBox6.Controls.Add(this.lblUsuario);
            this.groupBox6.Controls.Add(this.lblSenha);
            this.groupBox6.Location = new System.Drawing.Point(313, 186);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(219, 93);
            this.groupBox6.TabIndex = 51;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = " ";
            // 
            // txtUsuario
            // 
            this.txtUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtUsuario.ForeColor = System.Drawing.Color.Black;
            this.txtUsuario.Location = new System.Drawing.Point(62, 13);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(138, 21);
            this.txtUsuario.TabIndex = 3;
            // 
            // txtSenha
            // 
            this.txtSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.txtSenha.ForeColor = System.Drawing.Color.Black;
            this.txtSenha.Location = new System.Drawing.Point(62, 43);
            this.txtSenha.Name = "txtSenha";
            this.txtSenha.Size = new System.Drawing.Size(140, 21);
            this.txtSenha.TabIndex = 4;
            this.txtSenha.UseSystemPasswordChar = true;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblUsuario.ForeColor = System.Drawing.Color.Black;
            this.lblUsuario.Location = new System.Drawing.Point(11, 16);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(53, 15);
            this.lblUsuario.TabIndex = 5;
            this.lblUsuario.Text = "Usuário:";
            // 
            // lblSenha
            // 
            this.lblSenha.AutoSize = true;
            this.lblSenha.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblSenha.ForeColor = System.Drawing.Color.Black;
            this.lblSenha.Location = new System.Drawing.Point(11, 46);
            this.lblSenha.Name = "lblSenha";
            this.lblSenha.Size = new System.Drawing.Size(46, 15);
            this.lblSenha.TabIndex = 6;
            this.lblSenha.Text = "Senha:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.txtTelefone);
            this.groupBox4.Controls.Add(this.txtEmail);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Location = new System.Drawing.Point(13, 186);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(292, 93);
            this.groupBox4.TabIndex = 14;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Contato";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(80, 49);
            this.txtTelefone.Multiline = true;
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(197, 21);
            this.txtTelefone.TabIndex = 4;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(54, 14);
            this.txtEmail.Multiline = true;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(230, 21);
            this.txtEmail.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Telefone 1:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "Email:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtCidade);
            this.groupBox3.Controls.Add(this.txtBairro);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtRua);
            this.groupBox3.Controls.Add(this.txtNumero);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(11, 112);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(292, 73);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Endereço";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(190, 46);
            this.txtCidade.Multiline = true;
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(98, 22);
            this.txtCidade.TabIndex = 7;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(45, 46);
            this.txtBairro.Multiline = true;
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(93, 22);
            this.txtBairro.TabIndex = 6;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(144, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(49, 15);
            this.label7.TabIndex = 5;
            this.label7.Text = "Cidade:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 50);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Bairro:";
            // 
            // txtRua
            // 
            this.txtRua.Location = new System.Drawing.Point(39, 19);
            this.txtRua.Multiline = true;
            this.txtRua.Name = "txtRua";
            this.txtRua.Size = new System.Drawing.Size(164, 21);
            this.txtRua.TabIndex = 3;
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(229, 19);
            this.txtNumero.Multiline = true;
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(57, 21);
            this.txtNumero.TabIndex = 2;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(206, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 15);
            this.label5.TabIndex = 1;
            this.label5.Text = "Nº:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "Rua:";
            // 
            // dtpNascimento
            // 
            this.dtpNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpNascimento.Location = new System.Drawing.Point(130, 79);
            this.dtpNascimento.Name = "dtpNascimento";
            this.dtpNascimento.Size = new System.Drawing.Size(84, 21);
            this.dtpNascimento.TabIndex = 12;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 15);
            this.label3.TabIndex = 11;
            this.label3.Text = "Data de nascimento:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkF);
            this.groupBox2.Controls.Add(this.chkM);
            this.groupBox2.Location = new System.Drawing.Point(220, 68);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(83, 39);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Sexo";
            // 
            // chkF
            // 
            this.chkF.AutoSize = true;
            this.chkF.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkF.Location = new System.Drawing.Point(48, 15);
            this.chkF.Name = "chkF";
            this.chkF.Size = new System.Drawing.Size(31, 17);
            this.chkF.TabIndex = 1;
            this.chkF.TabStop = true;
            this.chkF.Text = "F";
            this.chkF.UseVisualStyleBackColor = true;
            // 
            // chkM
            // 
            this.chkM.AutoSize = true;
            this.chkM.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkM.Location = new System.Drawing.Point(6, 15);
            this.chkM.Name = "chkM";
            this.chkM.Size = new System.Drawing.Size(34, 17);
            this.chkM.TabIndex = 0;
            this.chkM.TabStop = true;
            this.chkM.Text = "M";
            this.chkM.UseVisualStyleBackColor = true;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(110, 21);
            this.txtNome.Multiline = true;
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(193, 20);
            this.txtNome.TabIndex = 9;
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(210, 47);
            this.txtCPF.Mask = "000000000/00";
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(93, 21);
            this.txtCPF.TabIndex = 8;
            // 
            // txtRG
            // 
            this.txtRG.Location = new System.Drawing.Point(40, 46);
            this.txtRG.Mask = "00,000,000-0";
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(90, 21);
            this.txtRG.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(171, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 15);
            this.label10.TabIndex = 6;
            this.label10.Text = "CPF:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(10, 50);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 15);
            this.label11.TabIndex = 5;
            this.label11.Text = "RG: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 15);
            this.label2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome Completo:";
            // 
            // UsuarioAlterar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 405);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.tabControl1);
            this.Name = "UsuarioAlterar";
            this.Text = "UsuarioAlterar";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.gpbEstoque.ResumeLayout(false);
            this.gpbEstoque.PerformLayout();
            this.gpbCompras.ResumeLayout(false);
            this.gpbCompras.PerformLayout();
            this.gpbFinanceiro.ResumeLayout(false);
            this.gpbFinanceiro.PerformLayout();
            this.gpbRH.ResumeLayout(false);
            this.gpbRH.PerformLayout();
            this.gpbFuncionario.ResumeLayout(false);
            this.gpbFuncionario.PerformLayout();
            this.gpbVendas.ResumeLayout(false);
            this.gpbVendas.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudConvenio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalario)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudVR)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem voltarAoMenuToolStripMenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox gpbEstoque;
        private System.Windows.Forms.CheckBox chkConsultarEstoque;
        private System.Windows.Forms.CheckBox chkSalvarEstoque;
        private System.Windows.Forms.CheckBox chkAlterarEstoque;
        private System.Windows.Forms.CheckBox chkRemoverEstoque;
        private System.Windows.Forms.GroupBox gpbCompras;
        private System.Windows.Forms.CheckBox chkConsultarCompras;
        private System.Windows.Forms.CheckBox chkSalvarCompras;
        private System.Windows.Forms.CheckBox chkAlterarCompras;
        private System.Windows.Forms.CheckBox chkRemoverCompras;
        private System.Windows.Forms.GroupBox gpbFinanceiro;
        private System.Windows.Forms.CheckBox chkConsultarFinanceiro;
        private System.Windows.Forms.CheckBox chkSalvarFinanceiro;
        private System.Windows.Forms.CheckBox chkAlterarFinanceiro;
        private System.Windows.Forms.CheckBox chkRemoverFinanceiro;
        private System.Windows.Forms.GroupBox gpbRH;
        private System.Windows.Forms.CheckBox chkConsultarRH;
        private System.Windows.Forms.CheckBox chkSalvarRH;
        private System.Windows.Forms.CheckBox chkAlterarRH;
        private System.Windows.Forms.CheckBox chkRemoverRH;
        private System.Windows.Forms.GroupBox gpbFuncionario;
        private System.Windows.Forms.CheckBox chkConsultarFuncionario;
        private System.Windows.Forms.CheckBox chkSalvarFuncionario;
        private System.Windows.Forms.CheckBox chkAlterarFuncionario;
        private System.Windows.Forms.CheckBox chkRemoverFuncionario;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.GroupBox gpbVendas;
        private System.Windows.Forms.CheckBox chkConsultarVendas;
        private System.Windows.Forms.CheckBox chkSalvarVendas;
        private System.Windows.Forms.CheckBox chkAlterarVendas;
        private System.Windows.Forms.CheckBox chkRemoverVendas;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudConvenio;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.NumericUpDown nudSalario;
        private System.Windows.Forms.NumericUpDown nudVT;
        private System.Windows.Forms.NumericUpDown nudVR;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtSenha;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblSenha;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtTelefone;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtRua;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpNascimento;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton chkF;
        private System.Windows.Forms.RadioButton chkM;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.MaskedTextBox txtCPF;
        private System.Windows.Forms.MaskedTextBox txtRG;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}