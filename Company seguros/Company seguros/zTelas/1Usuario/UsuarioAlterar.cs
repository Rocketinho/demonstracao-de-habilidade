﻿using Company_seguros.DB.Funcionario;
using Company_seguros.DB.Funcionario.Permissão;
using Company_seguros.Ferramentas.VoltarDepartamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._1Usuario
{
    public partial class UsuarioAlterar : Form
    {
        public UsuarioAlterar()
        {
            InitializeComponent();
        }
        //O único erro estava aqui!
        DTO_Funcionario dto;
        int id_funcionario;

        public void Loadscreen(DTO_Funcionario dto, int id_funcionario)

        {
            this.dto = dto;
            this.id_funcionario = id_funcionario;
            txtNome.Text = dto.Nome;
            txtRG.Text = dto.RG;
            txtCPF.Text = dto.CPF;
            dtpNascimento.Value = dto.Nascimento;
            if (dto.Sexo == "F")
            {
                chkF.Checked = true;
            }
            if (dto.Sexo == "M")
            {
                chkM.Checked = true;
            }
            nudVT.Value = dto.Transporte;
            nudVR.Value = dto.Refeicao;
            nudVA.Value = dto.Alimentacao;
            nudConvenio.Value = dto.Convenio;
            nudSalario.Value = dto.Salario;
            txtRua.Text = dto.Rua;
            txtBairro.Text = dto.Bairro;
            txtCidade.Text = dto.Cidade;
            txtNumero.Text = dto.Número;
            txtEmail.Text = dto.Email;

            
           
            txtTelefone.Text = dto.Telefone;
   
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {



                //Não era necessário utilizar o CONSTRUTOR THIS, pois o único problema era que você tinha instânciado a variável 
                //DTO, desta forma, a mesma não poderia ser utilizada em todos os métodos!
                dto.Nome = txtNome.Text;

                // if para n salvar RG só com a Mask (Não mexer para n afetar outra parte)
                if (txtRG.Text == "  .   .   -")
                {
                    txtRG.Text = null;
                }
                else
                {
                    dto.RG = txtRG.Text;
                }
                // if para n salvar CPF só com a Mask (Não mexer para n afetar outra parte)


                if (txtRG.Text == "  .   .   /    -")
                {
                    txtCPF.Text = null;
                }
                else
                {
                    dto.CPF = txtCPF.Text;
                }

                dto.Nascimento = dtpNascimento.Value;
                if (chkF.Checked == true)
                {
                    dto.Sexo = chkF.Text.ToString();
                }
                if (chkM.Checked == true)
                {
                    dto.Sexo = chkM.Text.ToString();
                }
                dto.Transporte = nudVT.Value;
                dto.Refeicao = nudVR.Value;
                dto.Alimentacao = nudVA.Value;
                dto.Convenio = nudConvenio.Value;
                dto.Salario = nudSalario.Value;
                dto.Bairro = txtBairro.Text;
                dto.Rua = txtRua.Text;
                dto.Email = txtEmail.Text;
                dto.Cidade = txtCidade.Text;

                dto.Número = txtNumero.Text;

                dto.Telefone = txtTelefone.Text;
                dto.Usuario = txtUsuario.Text;
                dto.Senha = txtSenha.Text;

                Business_Funcionario business = new Business_Funcionario();
                business.Alterar(dto);

                MessageBox.Show("Salvo com Sucesso!", "Company-Seguros", MessageBoxButtons.OK, MessageBoxIcon.Information);

                DTO_Acesso dto_acesso = new DTO_Acesso();
                dto_acesso.ID_Funcionario = id_funcionario;

                Business_Acesso b = new Business_Acesso();
                b.Remover(dto_acesso);

                if (chkSalvarVendas.Checked == true || chkRemoverVendas.Checked == true || chkConsultarVendas.Checked == true || chkAlterarVendas.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "Vendas";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarVendas.Checked;
                    dt.Remover = chkRemoverVendas.Checked;
                    dt.Consultar = chkConsultarVendas.Checked;
                    dt.Alterar = chkAlterarVendas.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka permissões em Funcionários
                if (chkSalvarFuncionario.Checked == true || chkRemoverFuncionario.Checked == true || chkConsultarFuncionario.Checked == true || chkAlterarFuncionario.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "Funcionários";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarFuncionario.Checked;
                    dt.Remover = chkRemoverFuncionario.Checked;
                    dt.Consultar = chkConsultarFuncionario.Checked;
                    dt.Alterar = chkAlterarFuncionario.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de RH
                if (chkSalvarRH.Checked == true || chkRemoverRH.Checked == true || chkConsultarRH.Checked == true || chkAlterarRH.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "RH";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarRH.Checked;
                    dt.Remover = chkRemoverRH.Checked;
                    dt.Consultar = chkConsultarRH.Checked;
                    dt.Alterar = chkAlterarRH.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões do Financeiro
                if (chkSalvarFinanceiro.Checked == true || chkRemoverFinanceiro.Checked == true || chkConsultarFinanceiro.Checked == true || chkAlterarFinanceiro.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "Financeiro";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarFinanceiro.Checked;
                    dt.Remover = chkRemoverFinanceiro.Checked;
                    dt.Consultar = chkConsultarFinanceiro.Checked;
                    dt.Alterar = chkAlterarFinanceiro.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de Compras
                if (chkSalvarCompras.Checked == true || chkRemoverCompras.Checked == true || chkConsultarCompras.Checked == true || chkAlterarCompras.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "Compras";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarCompras.Checked;
                    dt.Remover = chkRemoverCompras.Checked;
                    dt.Consultar = chkConsultarCompras.Checked;
                    dt.Alterar = chkAlterarCompras.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);

                }
                //Checka Permissões de Estoque
                if (chkSalvarEstoque.Checked == true || chkRemoverEstoque.Checked == true || chkConsultarEstoque.Checked == true || chkAlterarEstoque.Checked == true)
                {

                    DepartamentoDTO deteo = new DepartamentoDTO();
                    deteo.Departamento = "Estoque";
                    // Pesquisa departamento e volta o id

                    VoltarDepartamento departamento = new VoltarDepartamento();
                    int id_departamento = departamento.PesquisarID(deteo);

                    // Passa Valores pro DTO

                    DTO_Acesso dt = new DTO_Acesso();
                    dt.ID_Funcionario = id_funcionario;
                    dt.ID_Departamento = id_departamento;
                    dt.Salvar = chkSalvarEstoque.Checked;
                    dt.Remover = chkRemoverEstoque.Checked;
                    dt.Consultar = chkConsultarEstoque.Checked;
                    dt.Alterar = chkAlterarEstoque.Checked;

                    Business_Acesso acesso = new Business_Acesso();
                    acesso.Salvar(dt);


                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
    }
}
