﻿using Company_seguros.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._2Cliente
{
    public partial class BuscarCliente : Form
    {
        public BuscarCliente()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void cadrastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
        private void Consultar(DTO_Cliente dto)
        {  
            dto.CPF = txtCPF.Text;
            dto.Nome = txtnome.Text;
            Business_Cliente business = new Business_Cliente();
            List<DTO_Cliente> lista = business.Consultar(dto);
            dgvCliente.AutoGenerateColumns = false;
            dgvCliente.DataSource = lista;
             
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Consultar(dto);
        }
    }
}
