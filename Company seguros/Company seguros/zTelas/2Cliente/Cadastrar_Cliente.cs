﻿using Company_seguros.DB._base;
using Company_seguros.DB.Cliente;
using Company_seguros.Ferramentas;
using Company_seguros.Ferramentas.Localizacao;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas.Cliente
{
    public partial class Cadastrar_Cliente : Form
    {
        public Cadastrar_Cliente()
        {
            InitializeComponent();
            CarregarCombos();
        }

        private void CarregarCombos()
        {
            this.HandleError(() =>
            {
                Localizacao localizacao = new Localizacao();
                cboUF.DataSource = localizacao.UF();
            });
        }
        

        private void label10_Click(object sender, EventArgs e)
        {
            
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void Cadrastrar_cliente_Load(object sender, EventArgs e)
        {

        }
        private void Salvar(DTO_Cliente dto)
        {
            try
            {
                Sexo sexo = new Sexo();

                dto.Nome = txtNome.Text;
                dto.RG = "";
                dto.CPF = txtCPF.Text;
                dto.CNPJ = txtCNPJ.Text;
                dto.Nascimento = dtpNascimento.Value;
                bool masculino = chkMasculino.Checked;
                bool feminino = chkFeminino.Checked;
                dto.Sexo = sexo.VerificarSexo(masculino, feminino);
                dto.Endereco = txtEndereco.Text;
                dto.Numero = txtNumero.Text;
                dto.Cidade = txtCidade.Text;
                dto.UF = cboUF.SelectedItem.ToString();
                dto.Complemento = txtComplemento.Text;
                dto.Email = txtEmail.Text;
                dto.Telefone = txtTelefone.Text;

                Business_Cliente business = new Business_Cliente();
                business.Salvar(dto);
                
                MessageBox.Show("Salvo com sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch 
            {
                MessageBox.Show("Verifique se os dados estão corretos", "Company", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        private void button1_Click(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Salvar(dto);
        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void Cadastrar_Cliente_Enter(object sender, EventArgs e)
        {
            DTO_Cliente dto = new DTO_Cliente();
            Salvar(dto);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DTO_Cliente dto = new DTO_Cliente();
                Salvar(dto);
            }
        }

        private void cboCidade_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cboUF_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }



    interface IBase
    {
        int Salvar(string script, List<MySqlParameter> parms);
        void Alterar(string script, List<MySqlParameter> parms);
        void Delete(string script, List<MySqlParameter> parms);
    }

    interface ICrud<T>
    {
        int Salvar(T dto);
        void Alterar(T dto);
        void Delete(T dto);
    }

    abstract class Base<T> : IBase
    {
        public void Alterar(string script, List<MySqlParameter> parms)
        {
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public void Delete(string script, List<MySqlParameter> parms)
        {
            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public int Salvar(string script, List<MySqlParameter> parms)
        {
            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }
    }


    class ClienteDatabase : Base<DTO_Cliente>, ICrud<DTO_Cliente>
    {
        public void Alterar(DTO_Cliente dto)
        {
            string script = "update ....";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            base.Alterar(script, parms);
        }

        public void Delete(DTO_Cliente dto)
        {
            string script = "delete ....";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            base.Delete(script, parms);
        }

        public int Salvar(DTO_Cliente dto)
        {
            string script = "insert....";
            List<MySqlParameter> parms = new List<MySqlParameter>();

            return base.Salvar(script, parms);
        }
    }


}
