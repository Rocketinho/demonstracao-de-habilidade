﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class CadastrarProdutoVenda : Form
    {
        public CadastrarProdutoVenda()
        {
            InitializeComponent();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu mn = new frmMenu();
            mn.Show();
            Hide();
        }
    }
}
