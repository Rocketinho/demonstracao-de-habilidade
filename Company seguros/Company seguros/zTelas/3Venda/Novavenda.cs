﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._3Venda
{
    public partial class Novavenda : Form
    {
        public Novavenda()
        {
            InitializeComponent();
        }

        private void produtosAVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosVenda tela = new ProdutosVenda();
            tela.Show();
            Hide();
        }

        private void addNovoProdutoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarProdutoVenda tela = new CadastrarProdutoVenda();
            tela.Show();
            Hide();
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
