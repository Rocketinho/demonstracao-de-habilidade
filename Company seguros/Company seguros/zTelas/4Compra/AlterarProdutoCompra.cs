﻿using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.DB.MODULO2.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class AlterarProdutoCompra : Form
    {
        public AlterarProdutoCompra()
        {
            InitializeComponent();
        }
        //Business de escopo
        Business_Produto db = new Business_Produto();

        //DTO de escopo
        DTO_Produto dto;

        //Pegando o valor de registro individual, selecionado pela gried no formulário de Consulta.
        public void LoadScreen(DTO_Produto dto)
        {
            //Fazendo o DTO de escopo receber o valor de parâmetro
            this.dto = dto;

            //Passando o valor para os controles
          
           
            txtProduto.Text = dto.Produto;
            txtMarca.Text = dto.Marca;
            nudPreco.Value = dto.Preco;
           
        }

        private void AlterarProdutoCompra_Load(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
          

            //Pegando o novo valor do DTO
     
            this.dto.Produto = txtProduto.Text;
           this.dto.Marca = txtMarca.Text;
            this.dto.Preco = nudPreco.Value;
           

            //Chamando o método de alterar
            Business_Produto db = new Business_Produto();
            db.Alterar(dto);

            MessageBox.Show( "Produto Alterado com Sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);

            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }
    }
}
