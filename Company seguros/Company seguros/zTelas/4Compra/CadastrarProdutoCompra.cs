﻿using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.DB.MODULO2.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class CadastrarProdutoCompra : Form
    {
        Business_Produto db = new Business_Produto();

        public CadastrarProdutoCompra()
        {
            InitializeComponent();
                //CarregarCombos();
        }
        //private void CarregarCombos()
        //{
        //    Business_Fornecedor db = new Business_Fornecedor();
        //    List<DTO_Fornecedor> list = db.Listar();

        //    cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
        //    cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);

        //    cboFornecedor.DataSource = list;
        //}
        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void produtosAVendaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ProdutosCompra tela = new ProdutosCompra();

        }
        public void Salvar(DTO_Produto dto)
        {
            //Pegando o DTO do Fornecedor

            //Passo o valor dos controles para o DTO.
          
            dto.Produto = txtProduto.Text;
            dto.Preco = nudPreco.Value;
            dto.Marca = txtMarca.Text;
           

            //Passo o valor do DTO para business.
            db.Salvar(dto);          
            MessageBox.Show( "Produto Salvo com Sucesso!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
           // Passo o valor do DTO para o método Salvar
            DTO_Produto dto = new DTO_Produto();
            Salvar(dto);
        }
    }
}
