﻿using Company_seguros.DB.Cliente;
using Company_seguros.DB.Funcionario;
using Company_seguros.DB.MODULO2.Compra;
using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.DB.MODULO2.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._4Compra
{
    public partial class NovaCompra : Form
    {
        BindingList<DTO_Produto> produtosCarrinho = new BindingList<DTO_Produto>();

        public NovaCompra()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();


        }

        void CarregarCombos()
        {
            Business_Produto business = new Business_Produto();
            List<DTO_Produto> lista = business.Listar();

            cboProduto.ValueMember = nameof(DTO_Produto.ID);
            cboProduto.DisplayMember = nameof(DTO_Produto.Produto);
            cboProduto.DataSource = lista;

            //Fornecedor
            Business_Fornecedor business_fornecedor = new Business_Fornecedor();
            List<DTO_Fornecedor> lista_fornecedor = business_fornecedor.Listar();

            cboFornecedor.ValueMember = nameof(DTO_Fornecedor.ID);
            cboFornecedor.DisplayMember = nameof(DTO_Fornecedor.Razao_Social);
            cboFornecedor.DataSource = lista_fornecedor;

            //Funcionario

            //Fornecedor
            Business_Funcionario business_funcionario = new Business_Funcionario();
            List<DTO_Funcionario> lista3 = business_funcionario.Listar();

            cboFuncionario.ValueMember = nameof(DTO_Funcionario.Id);
            cboFuncionario.DisplayMember = nameof(DTO_Funcionario.Nome);
            cboFuncionario.DataSource = lista3;

        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        private void voltarAoMenuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void cboProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
       

        private void button1_Click(object sender, EventArgs e)
        {
           

            // salva na compra
            Business_Compra busi = new Business_Compra();
            // pegar id do produto

            DTO_Produto produto = cboProduto.SelectedItem as DTO_Produto;

            // pegar id do fornecedor
            DTO_Fornecedor fornecedor = cboFornecedor.SelectedItem as DTO_Fornecedor;

            //pegar id do funcionario
            DTO_Funcionario funcionario = cboFuncionario.SelectedItem as DTO_Funcionario;

            DTO_Compra dt = new DTO_Compra();

            dt.ID_Produto = produto.ID;
            dt.ID_Fornecedor = fornecedor.ID;
            dt.Lote = txtLote.Text;
            dt.Validade = dtpValidade.Value;
            dt.Data_Compra = dtpCompra.Value;
            dt.ID_Funcionario = funcionario.Id;

            busi.Salvar(dt);

            MessageBox.Show("Compra bem sucedida");
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            DTO_Produto dto = cboProduto.SelectedItem as DTO_Produto;
          
            int qtd = Convert.ToInt32(txtQuantidade.Text);

            for (int i = 0; i < qtd; i++)
            {
                produtosCarrinho.Add(dto);
            }
        }

        private void cboFornecedor_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgvItens_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void cboFuncionario_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
