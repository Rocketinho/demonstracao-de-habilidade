﻿using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.Ferramentas.Localizacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._6Fornecedor
{
    public partial class AlterarFornecedor : Form
    {
        public AlterarFornecedor()
        {
            InitializeComponent();
            Localizacao uf = new Localizacao();
            cboEstado.DataSource = uf.UF();
        }


        DTO_Fornecedor dto;

        public void LoadScreen(DTO_Fornecedor dto)
        {
            this.dto = dto;

            
            txtNome.Text = dto.Razao_Social;
            txtCNPJ.Text = dto.CNPJ;
            txtTelefone.Text = dto.Telefone;
            txtEmail.Text = dto.Email;
            txtCEP.Text = dto.CEP;
            txtEndereco.Text = dto.Endereco;
            txtComplemento.Text = dto.Complemento;
            txtNumero.Text = dto.Numero_Casa.ToString();
            cboEstado.SelectedItem = dto.UF;
            txtCidade.Text = dto.Cidade;

        }
        public void Alterar(DTO_Fornecedor dto)
        {
            dto.Razao_Social = txtNome.Text;
            dto.CNPJ = txtCNPJ.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Email = txtEmail.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Numero_Casa = Convert.ToInt32(txtNumero.Text);
            dto.UF = cboEstado.SelectedItem.ToString();
            dto.Cidade = txtCidade.Text;

            Business_Fornecedor db = new Business_Fornecedor();
            db.Alterar(dto);

            MessageBox.Show("Registro alterado com sucesso!!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Alterar(dto);
        }
    }
}
