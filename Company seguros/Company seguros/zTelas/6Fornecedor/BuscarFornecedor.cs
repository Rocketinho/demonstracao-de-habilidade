﻿using Company_seguros.DB.MODULO2.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._6Fornecedor
{
    public partial class BuscarFornecedor : Form
    {
        public BuscarFornecedor()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void cadrastrarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }
        public void CarregarGrid()
        {
            DTO_Fornecedor dto = new DTO_Fornecedor();
            dto.Razao_Social = txtNome.Text;

            Business_Fornecedor db = new Business_Fornecedor();
            List<DTO_Fornecedor> consult = db.Consultar(dto);

            dgvCompra.AutoGenerateColumns = false;
            dgvCompra.DataSource = consult;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            CarregarGrid();
        }

        private void dgvCompra_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == 4)
            {
                DTO_Fornecedor linha = dgvCompra.CurrentRow.DataBoundItem as DTO_Fornecedor;
                AlterarFornecedor tela = new AlterarFornecedor();
                tela.LoadScreen(linha);

                tela.Show();
            }
            if (e.ColumnIndex == 5)
            {               
              DialogResult r =  MessageBox.Show("Deseja realmente excluir?", "Company", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if(r == DialogResult.Yes)
                {
                    DTO_Fornecedor linha = dgvCompra.CurrentRow.DataBoundItem as DTO_Fornecedor;

                    Business_Fornecedor db = new Business_Fornecedor();
                    db.Remover(linha.ID);                   
                    CarregarGrid();

                }
                
            }
        }
    }
}
