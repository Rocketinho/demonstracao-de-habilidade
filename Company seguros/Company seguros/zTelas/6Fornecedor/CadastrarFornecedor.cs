﻿using Company_seguros.DB.MODULO2.Fornecedor;
using Company_seguros.Ferramentas.Localizacao;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Company_seguros.zTelas._6Fornecedor
{
    public partial class CadastrarFornecedor : Form
    {
        public CadastrarFornecedor()
        {
            InitializeComponent();
            Localizacao uf = new Localizacao();
            cboEstado.DataSource = uf.UF();
        }

        private void consultarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void alterarClienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMenu menu = new frmMenu();
            menu.Show();
            this.Close();
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }
        public void Salvar(DTO_Fornecedor dto)
        {
            //Pego o DTO como Parâmetro, após isso, passo o valor encontrado nos controles para o DTO
            dto.Razao_Social = txtNome.Text;
            dto.CNPJ = txtCNPJ.Text;
            dto.Telefone = txtTelefone.Text;
            dto.CEP = txtCEP.Text;
            dto.Endereco = txtEndereco.Text;
            dto.Complemento = txtComplemento.Text;
            dto.Cidade = txtCidade.Text;
            dto.Email = txtEmail.Text;
            dto.Numero_Casa = Convert.ToInt32(txtNumero.Text);
            dto.UF = cboEstado.SelectedItem.ToString();

            //Chamo a busines e passo o valor do DTO para o método Salvar
            Business_Fornecedor db = new Business_Fornecedor();
            db.Salvar(dto);

            MessageBox.Show("Fornecedor salvo com sucesso!!", "Company", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Instacio o DTO e chamo o método Salvar.
            DTO_Fornecedor dto = new DTO_Fornecedor();
            Salvar(dto);
        }
    }
}
