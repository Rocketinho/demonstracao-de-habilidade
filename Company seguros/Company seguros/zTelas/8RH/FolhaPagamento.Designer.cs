﻿namespace Company_seguros.zTelas._8RH
{
    partial class FolhaPagamento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVoltar = new System.Windows.Forms.Button();
            this.gpbFolhaPagamento = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.nudPlanoSaude = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.rdnNaoS = new System.Windows.Forms.RadioButton();
            this.rdnSimS = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nudVA = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.rdnNaoA = new System.Windows.Forms.RadioButton();
            this.rdnSimA = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.nudSalarioHora = new System.Windows.Forms.NumericUpDown();
            this.nudDiasTrabalhados = new System.Windows.Forms.NumericUpDown();
            this.lblSalario = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cboCargo = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHorasTrabalhadas = new System.Windows.Forms.TextBox();
            this.txtNomeFuncionario = new System.Windows.Forms.TextBox();
            this.btnBuscarFuncionario = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnGerarPagamento = new System.Windows.Forms.Button();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.voltarAoMenuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gpbFolhaPagamento.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoSaude)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioHora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiasTrabalhados)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.Gray;
            this.btnVoltar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVoltar.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVoltar.ForeColor = System.Drawing.Color.White;
            this.btnVoltar.Location = new System.Drawing.Point(480, 416);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(103, 27);
            this.btnVoltar.TabIndex = 31;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // gpbFolhaPagamento
            // 
            this.gpbFolhaPagamento.Controls.Add(this.panel2);
            this.gpbFolhaPagamento.Controls.Add(this.panel1);
            this.gpbFolhaPagamento.Controls.Add(this.nudSalarioHora);
            this.gpbFolhaPagamento.Controls.Add(this.nudDiasTrabalhados);
            this.gpbFolhaPagamento.Controls.Add(this.lblSalario);
            this.gpbFolhaPagamento.Controls.Add(this.label1);
            this.gpbFolhaPagamento.Controls.Add(this.cboCargo);
            this.gpbFolhaPagamento.Controls.Add(this.label9);
            this.gpbFolhaPagamento.Controls.Add(this.label10);
            this.gpbFolhaPagamento.Controls.Add(this.label8);
            this.gpbFolhaPagamento.Controls.Add(this.txtHorasTrabalhadas);
            this.gpbFolhaPagamento.Controls.Add(this.txtNomeFuncionario);
            this.gpbFolhaPagamento.Controls.Add(this.btnBuscarFuncionario);
            this.gpbFolhaPagamento.Controls.Add(this.label4);
            this.gpbFolhaPagamento.Controls.Add(this.label3);
            this.gpbFolhaPagamento.Controls.Add(this.btnGerarPagamento);
            this.gpbFolhaPagamento.Controls.Add(this.btnCalcular);
            this.gpbFolhaPagamento.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpbFolhaPagamento.ForeColor = System.Drawing.Color.Black;
            this.gpbFolhaPagamento.Location = new System.Drawing.Point(12, 27);
            this.gpbFolhaPagamento.Name = "gpbFolhaPagamento";
            this.gpbFolhaPagamento.Size = new System.Drawing.Size(571, 386);
            this.gpbFolhaPagamento.TabIndex = 30;
            this.gpbFolhaPagamento.TabStop = false;
            this.gpbFolhaPagamento.Text = "Folha de Pagamento ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.nudPlanoSaude);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.rdnNaoS);
            this.panel2.Controls.Add(this.rdnSimS);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Location = new System.Drawing.Point(16, 215);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(342, 54);
            this.panel2.TabIndex = 122;
            // 
            // nudPlanoSaude
            // 
            this.nudPlanoSaude.DecimalPlaces = 2;
            this.nudPlanoSaude.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPlanoSaude.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudPlanoSaude.Location = new System.Drawing.Point(30, 24);
            this.nudPlanoSaude.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudPlanoSaude.Name = "nudPlanoSaude";
            this.nudPlanoSaude.Size = new System.Drawing.Size(114, 26);
            this.nudPlanoSaude.TabIndex = 121;
            this.nudPlanoSaude.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.nudPlanoSaude.ValueChanged += new System.EventHandler(this.nudPlanoSaude_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(2, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 20);
            this.label7.TabIndex = 120;
            this.label7.Text = "R$:";
            // 
            // rdnNaoS
            // 
            this.rdnNaoS.AutoSize = true;
            this.rdnNaoS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnNaoS.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnNaoS.ForeColor = System.Drawing.Color.Black;
            this.rdnNaoS.Location = new System.Drawing.Point(262, 4);
            this.rdnNaoS.Name = "rdnNaoS";
            this.rdnNaoS.Size = new System.Drawing.Size(51, 24);
            this.rdnNaoS.TabIndex = 119;
            this.rdnNaoS.TabStop = true;
            this.rdnNaoS.Text = "Não";
            this.rdnNaoS.UseVisualStyleBackColor = true;
            // 
            // rdnSimS
            // 
            this.rdnSimS.AutoSize = true;
            this.rdnSimS.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnSimS.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnSimS.ForeColor = System.Drawing.Color.Black;
            this.rdnSimS.Location = new System.Drawing.Point(201, 4);
            this.rdnSimS.Name = "rdnSimS";
            this.rdnSimS.Size = new System.Drawing.Size(52, 24);
            this.rdnSimS.TabIndex = 118;
            this.rdnSimS.TabStop = true;
            this.rdnSimS.Text = "Sim";
            this.rdnSimS.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(1, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(166, 20);
            this.label6.TabIndex = 117;
            this.label6.Text = "Possui Plano de Saude ?";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.nudVA);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.rdnNaoA);
            this.panel1.Controls.Add(this.rdnSimA);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(16, 285);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(342, 56);
            this.panel1.TabIndex = 118;
            // 
            // nudVA
            // 
            this.nudVA.DecimalPlaces = 2;
            this.nudVA.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudVA.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudVA.Location = new System.Drawing.Point(30, 26);
            this.nudVA.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudVA.Name = "nudVA";
            this.nudVA.Size = new System.Drawing.Size(114, 26);
            this.nudVA.TabIndex = 122;
            this.nudVA.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(1, 28);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(29, 20);
            this.label12.TabIndex = 121;
            this.label12.Text = "R$:";
            // 
            // rdnNaoA
            // 
            this.rdnNaoA.AutoSize = true;
            this.rdnNaoA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnNaoA.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnNaoA.ForeColor = System.Drawing.Color.Black;
            this.rdnNaoA.Location = new System.Drawing.Point(262, 5);
            this.rdnNaoA.Name = "rdnNaoA";
            this.rdnNaoA.Size = new System.Drawing.Size(51, 24);
            this.rdnNaoA.TabIndex = 120;
            this.rdnNaoA.TabStop = true;
            this.rdnNaoA.Text = "Não";
            this.rdnNaoA.UseVisualStyleBackColor = true;
            // 
            // rdnSimA
            // 
            this.rdnSimA.AutoSize = true;
            this.rdnSimA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdnSimA.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdnSimA.ForeColor = System.Drawing.Color.Black;
            this.rdnSimA.Location = new System.Drawing.Point(201, 5);
            this.rdnSimA.Name = "rdnSimA";
            this.rdnSimA.Size = new System.Drawing.Size(52, 24);
            this.rdnSimA.TabIndex = 119;
            this.rdnSimA.TabStop = true;
            this.rdnSimA.Text = "Sim";
            this.rdnSimA.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(1, 7);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(176, 20);
            this.label11.TabIndex = 118;
            this.label11.Text = "Possui Vale Alimentação ?";
            // 
            // nudSalarioHora
            // 
            this.nudSalarioHora.DecimalPlaces = 2;
            this.nudSalarioHora.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudSalarioHora.Increment = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nudSalarioHora.Location = new System.Drawing.Point(22, 182);
            this.nudSalarioHora.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.nudSalarioHora.Name = "nudSalarioHora";
            this.nudSalarioHora.Size = new System.Drawing.Size(138, 26);
            this.nudSalarioHora.TabIndex = 116;
            this.nudSalarioHora.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudDiasTrabalhados
            // 
            this.nudDiasTrabalhados.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudDiasTrabalhados.Location = new System.Drawing.Point(212, 183);
            this.nudDiasTrabalhados.Name = "nudDiasTrabalhados";
            this.nudDiasTrabalhados.Size = new System.Drawing.Size(138, 26);
            this.nudDiasTrabalhados.TabIndex = 116;
            this.nudDiasTrabalhados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSalario
            // 
            this.lblSalario.AutoSize = true;
            this.lblSalario.ForeColor = System.Drawing.Color.Black;
            this.lblSalario.Location = new System.Drawing.Point(42, 361);
            this.lblSalario.Name = "lblSalario";
            this.lblSalario.Size = new System.Drawing.Size(17, 20);
            this.lblSalario.TabIndex = 115;
            this.lblSalario.Text = "--";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(17, 361);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 20);
            this.label1.TabIndex = 115;
            this.label1.Text = "R$:";
            // 
            // cboCargo
            // 
            this.cboCargo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cboCargo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCargo.Enabled = false;
            this.cboCargo.ForeColor = System.Drawing.Color.Black;
            this.cboCargo.FormattingEnabled = true;
            this.cboCargo.Location = new System.Drawing.Point(22, 112);
            this.cboCargo.Name = "cboCargo";
            this.cboCargo.Size = new System.Drawing.Size(138, 28);
            this.cboCargo.TabIndex = 105;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(208, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(121, 20);
            this.label9.TabIndex = 100;
            this.label9.Text = "Dias Trabalhados:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(398, 163);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(129, 20);
            this.label10.TabIndex = 91;
            this.label10.Text = "Horas Trabalhadas:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(17, 163);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(90, 20);
            this.label8.TabIndex = 89;
            this.label8.Text = "Salario Hora:";
            // 
            // txtHorasTrabalhadas
            // 
            this.txtHorasTrabalhadas.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHorasTrabalhadas.ForeColor = System.Drawing.Color.Black;
            this.txtHorasTrabalhadas.Location = new System.Drawing.Point(402, 183);
            this.txtHorasTrabalhadas.MaxLength = 50;
            this.txtHorasTrabalhadas.Name = "txtHorasTrabalhadas";
            this.txtHorasTrabalhadas.Size = new System.Drawing.Size(138, 26);
            this.txtHorasTrabalhadas.TabIndex = 87;
            // 
            // txtNomeFuncionario
            // 
            this.txtNomeFuncionario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNomeFuncionario.ForeColor = System.Drawing.Color.Black;
            this.txtNomeFuncionario.Location = new System.Drawing.Point(22, 48);
            this.txtNomeFuncionario.MaxLength = 50;
            this.txtNomeFuncionario.Name = "txtNomeFuncionario";
            this.txtNomeFuncionario.Size = new System.Drawing.Size(489, 26);
            this.txtNomeFuncionario.TabIndex = 87;
            // 
            // btnBuscarFuncionario
            // 
            this.btnBuscarFuncionario.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBuscarFuncionario.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBuscarFuncionario.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscarFuncionario.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarFuncionario.ForeColor = System.Drawing.Color.Black;
            this.btnBuscarFuncionario.Location = new System.Drawing.Point(517, 49);
            this.btnBuscarFuncionario.Name = "btnBuscarFuncionario";
            this.btnBuscarFuncionario.Size = new System.Drawing.Size(32, 26);
            this.btnBuscarFuncionario.TabIndex = 86;
            this.btnBuscarFuncionario.UseVisualStyleBackColor = true;
            this.btnBuscarFuncionario.Click += new System.EventHandler(this.btnBuscarFuncionario_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Enabled = false;
            this.label4.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(18, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 20);
            this.label4.TabIndex = 81;
            this.label4.Text = "Cargo:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(18, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(148, 20);
            this.label3.TabIndex = 80;
            this.label3.Text = "Nome do Funcionario:";
            // 
            // btnGerarPagamento
            // 
            this.btnGerarPagamento.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGerarPagamento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGerarPagamento.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGerarPagamento.ForeColor = System.Drawing.Color.Black;
            this.btnGerarPagamento.Location = new System.Drawing.Point(379, 358);
            this.btnGerarPagamento.Name = "btnGerarPagamento";
            this.btnGerarPagamento.Size = new System.Drawing.Size(186, 27);
            this.btnGerarPagamento.TabIndex = 28;
            this.btnGerarPagamento.Text = "Gerar Folha de Pagamento";
            this.btnGerarPagamento.UseVisualStyleBackColor = true;
            this.btnGerarPagamento.Click += new System.EventHandler(this.btnGerarPagamento_Click);
            // 
            // btnCalcular
            // 
            this.btnCalcular.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCalcular.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCalcular.Font = new System.Drawing.Font("Arial Narrow", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalcular.ForeColor = System.Drawing.Color.Black;
            this.btnCalcular.Location = new System.Drawing.Point(278, 358);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(95, 27);
            this.btnCalcular.TabIndex = 27;
            this.btnCalcular.Text = "Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.voltarAoMenuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(596, 24);
            this.menuStrip1.TabIndex = 32;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // voltarAoMenuToolStripMenuItem
            // 
            this.voltarAoMenuToolStripMenuItem.Name = "voltarAoMenuToolStripMenuItem";
            this.voltarAoMenuToolStripMenuItem.Size = new System.Drawing.Size(99, 20);
            this.voltarAoMenuToolStripMenuItem.Text = "Voltar ao menu";
            this.voltarAoMenuToolStripMenuItem.Click += new System.EventHandler(this.voltarAoMenuToolStripMenuItem_Click);
            // 
            // FolhaPagamento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 452);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.btnVoltar);
            this.Controls.Add(this.gpbFolhaPagamento);
            this.Name = "FolhaPagamento";
            this.Text = "FolhaPagamento";
            this.gpbFolhaPagamento.ResumeLayout(false);
            this.gpbFolhaPagamento.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPlanoSaude)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudVA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalarioHora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudDiasTrabalhados)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnVoltar;
        private System.Windows.Forms.GroupBox gpbFolhaPagamento;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown nudPlanoSaude;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdnNaoS;
        private System.Windows.Forms.RadioButton rdnSimS;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown nudVA;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.RadioButton rdnNaoA;
        private System.Windows.Forms.RadioButton rdnSimA;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown nudSalarioHora;
        private System.Windows.Forms.NumericUpDown nudDiasTrabalhados;
        private System.Windows.Forms.Label lblSalario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboCargo;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtHorasTrabalhadas;
        private System.Windows.Forms.TextBox txtNomeFuncionario;
        private System.Windows.Forms.Button btnBuscarFuncionario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnGerarPagamento;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem voltarAoMenuToolStripMenuItem;
    }
}